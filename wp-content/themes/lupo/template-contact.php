<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<!-- Google Maps -->
<div id="map_canvas" style="opacity: 1; width:100%;"></div>

<?php

/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>

<?php if (get_field("revolution_slider")) { putRevSlider(get_field("revolution_slider")); } ?>

<div class="contact-container">

	<!-- Post Title -->
	<div class="entry-header">
   		<h1 class="heading">
   			<?php the_title(); ?>
   		</h1>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">
		<?php the_content(); ?>		
		</div>	
	</div>
	<?php endwhile; ?>
	<?php wp_reset_query(); ?>

	<!-- Alerts -->	
   	<div class="entry drop-shadow curved">
   		<div class="contact-alerts"></div>
	</div>
	
	<!-- Contact Form  -->
	<form id="contact" action="contact.php">
		<div class="row">
			<div class="col-md-6">
				<input id="name" type="text" value="" placeholder="Name" name="name">
			</div>	
			<div class="col-md-6">
				<input id="email" type="text" value="" placeholder="Email" name="email">
			</div>
		</div>
		<textarea id="message" required="" rows="6" placeholder="Message" name="message"></textarea>
		<div class="contact-submit-wrapper">
			<a data-contact-url="<?php echo get_template_directory_uri(); ?>/contact.php" data-lat="<?php echo get_theme_mod('google_lat'); ?>" data-long="<?php echo get_theme_mod('google_long'); ?>" class="contact-submit" href="#">Send message <i class="fa fa-angle-right"></i></a>
		</div>
	</form>			

</div>

<?php get_footer(); ?>