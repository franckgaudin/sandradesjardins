/*global jQuery:false, $f:false, YT:false */

/*-----------------------------------------------------------------------------------*/
/*	Dropdown Menu
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {
		"use strict";
		jQuery('.mobile-nav-container .dropdown').click(function() {
	    	jQuery('.sub-menu', this).slideToggle(200);
	    });

	    // Append arrows to dropdowns
		jQuery( ".dropdown > a" ).append( "<i class='fa fa-angle-down'></i>");
	    
});
		
/*-----------------------------------------------------------------------------------*/
/* Remove empty P tags
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {
	"use strict";
	jQuery( 'p:empty' ).remove();
});
	
/*-----------------------------------------------------------------------------------*/
/* Mobile Navigation Toggle
/*-----------------------------------------------------------------------------------*/

jQuery('.mobile-nav-container .dropdown').click(function() {
	"use strict";
	jQuery('.sub-menu', this).slideToggle(300);
});

/*-----------------------------------------------------------------------------------*/
/* Flexslider (Gallery post format)
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {
"use strict";
jQuery('.widget-slider .flexslider').flexslider({
	animation: "slide",
	slideshow: false, 
	animationSpeed: 600, 
	easing: "swing", 
	directionNav: true,
	prevText: "",
	nextText: "",
	});
});

/*-----------------------------------------------------------------------------------*/
/* Initiate tooltips and popovers
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function(){
	"use strict";
	jQuery('[data-toggle="tooltip"]').tooltip();
	jQuery('[data-toggle="popover"]').popover();
	jQuery('[data-show="true"]').popover('show');
});

/*-----------------------------------------------------------------------------------*/
/* Select
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function(){
	"use strict";
	jQuery("select").selectBoxIt();
	jQuery( ".menu-left .dropdown > a" ).append( "<i class='fa fa-angle-right'></i>");
	jQuery( ".menu-right .dropdown > a" ).append( "<i class='fa fa-angle-left'></i>");
});

/*-----------------------------------------------------------------------------------*/
/* Contact
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function(){
	jQuery('.contact-submit').click(function() {
		"use strict";
		var name = jQuery("#name").val();
		var email = jQuery("#email").val();
		var message = jQuery("#message").val();
		var contact_url = jQuery(this).attr("data-contact-url");
		
		var dataString = 'name='+ name + '&email=' + email + '&message=' + message;
		
		jQuery.ajax({  
			type: "POST",  
			url: contact_url,  
			data: dataString,  
			success:  function (html) { 
				if (html === "invalid_email") {
					jQuery('.contact-alerts').empty();
					jQuery('.contact-alerts').append('<div id="email-error" class="alert alert-warning">This email is invalid!</div>');
					} else if (html === "success") {
						jQuery('.contact-alerts').empty();
						jQuery('.contact-alerts').append('<div id="email-success" class="alert alert-success">Your email has been sent!</div>');
					} else if (html === "error") {
						jQuery('.contact-alerts').empty();
						jQuery('.contact-alerts').append('<div id="email-error" class="alert alert-warning">Please fill out all the fields!</div>');
					}
				}  
		});  
	});
});

jQuery(document).ready(function(){

  	$container = jQuery('.isotope-container');
	$container.imagesLoaded( function(){
		$container.isotope({
			itemSelector : '.isotope-item',
				masonry: {
				columnWidth: 1
			}
	    });
	  	$container.isotope("layout").isotope();
	});
	jQuery('.isotope-container').infinitescroll({
	    navSelector  : '.isotope-loadmore',    // selector for the paged navigation 
	    nextSelector : '.isotope-loadmore a',  // selector for the NEXT link (to page 2)
	    itemSelector : '.isotope-item',     // selector for all items you'll retrieve
	    behavior : 'twitter',
	    loading: {
	        finishedMsg: '',
	        img: ''
	      }
	    },
	    // call Isotope as a callback
	    function( newElements ) {
	      jQuery('.isotope-container').isotope( 'appended', jQuery( newElements ) ); 
	      jQuery('.isotope-loadmore a').html("<i class='fa fa-plus'></i> Load more");
	    }
    );

	jQuery('.isotope-loadmore a').click(function() {
		jQuery('.isotope-loadmore a').html("<i class='fa fa-spinner fa-spin'></i>");
	});

	jQuery('.category-filter li').on( 'click', 'a', function() {
	  var filterValue = jQuery(this).attr('data-filter');
	  $container.isotope({ filter: filterValue });

	  return false;
	});
   
});


jQuery(document).ready(function(){
	if (jQuery('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = jQuery(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                jQuery('#back-to-top').addClass('show');
	            } else {
	                jQuery('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    jQuery(window).on('scroll', function () {
	        backToTop();
	    });
	    jQuery('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        jQuery('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

jQuery(document).ready(function(){

	document_height = jQuery(window).height() - jQuery("#wpadminbar").height();
	jQuery('#map_canvas').css("height", document_height);
	jQuery('.header-featured-image').css("height", document_height);
	jQuery('.header-featured-text').css("height", document_height/2);
	jQuery('.header-featured-text').css("line-height", document_height/2+"px");

	jQuery('.mb-fullscreen').css("height", document_height);

	var carousel_speed = jQuery(".carousel").attr("data-speed");
	var carousel_pause = jQuery(".carousel").attr("data-pause");

 	jQuery('.carousel').carousel({
        pause: carousel_pause
    })

    jQuery( window ).resize(function() {
		jQuery('#map_canvas').css("height", document_height);
		jQuery('.header-featured-image').css("height", document_height);
		jQuery('.header-featured-text').css("height", document_height/2);
		jQuery('.header-featured-text').css("line-height", document_height/2+"px");
		jQuery('.project-content').css("margin-top", document_height);

		jQuery('.mb-fullscreen').css("height", document_height);
	});

});

/*-----------------------------------------------------------------------------------*/
/* Contact
/*-----------------------------------------------------------------------------------*/

// Google Maps
function initialize() {

	var google_lat = jQuery(".contact-submit").attr("data-lat");
	var google_long = jQuery(".contact-submit").attr("data-long");

	var styles = [ { "stylers": [ { "visibility": "on" }, { "saturation": -100 }, { "gamma": 1 } ] },{ } ];
	
	var myLatlng = new google.maps.LatLng(google_lat, google_long );
    var mapOptions = {
	    zoom: 17,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    disableDefaultUI: true,
	    draggable: false,
	    scrollwheel: false,
	    center: myLatlng,
	};
	
	var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setOptions({styles: styles});
    
	var marker = new google.maps.Marker({
	    position: myLatlng,
	    map: map,
	});

}

/*-----------------------------------------------------------------------------------*/
/* Preloader
/*-----------------------------------------------------------------------------------*/

jQuery(window).load(function() {
	"use strict";
	jQuery('body').imagesLoaded( function(){
		jQuery(".preloader").css("opacity", "0");
		jQuery(".preloader").css("visibility", "hidden");
	});
});

/*-----------------------------------------------------------------------------------*/
/* Center
/*-----------------------------------------------------------------------------------*/

jQuery.fn.center = function () {
    this.css("top", Math.max(0, ((jQuery(window).height() - jQuery(this).outerHeight()) / 2) + jQuery(window).scrollTop()) + "px");
    return this;
}

jQuery(document).ready(function(){
	jQuery(".contact-container").center();
});

/*-----------------------------------------------------------------------------------*/
/* Carousel Swipe Support
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {  
	jQuery(".carousel").swiperight(function() {  
		jQuery(this).carousel('prev');  
    });  
	jQuery(".carousel").swipeleft(function() {  
		jQuery(this).carousel('next');  
	});  
});  

/*-----------------------------------------------------------------------------------*/
/* sticky
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {
	var sticky_header = jQuery(jQuery(".header-wrapper")).attr("data-sticky");

	if (sticky_header == 1) {

		header_height = jQuery(".header").outerHeight();
		jQuery(".content-container").css("margin-top", header_height);
		jQuery(".fullscreen-content-container").css("margin-top", header_height);

		jQuery(document).scroll(function() {
		  if (jQuery(document).scrollTop() >= 50) {
		    // user scrolled 50 pixels or more;
		    // do stuff
		    jQuery(".header-wrapper").addClass("header-sticky");
		  } else {
		    jQuery(".header-wrapper").removeClass("header-sticky");
		  }
		});

	}
});

/*-----------------------------------------------------------------------------------*/
/* Navigation Side
/*-----------------------------------------------------------------------------------*/

jQuery(document).ready(function() {
		"use strict";
		jQuery('.navigation-side-toggle').click(function() {
	    	jQuery('body').addClass('navigation-side-active');
	    });    

	    jQuery('.navigation-fullscreen-toggle').click(function() {
	    	jQuery('body').addClass('navigation-fullscreen-active');
	    }); 

	    jQuery('.navigation-side .close-button').click(function() {
	    	jQuery('body').removeClass('navigation-side-active');
	    }); 

	     jQuery('.navigation-fullscreen .close-button').click(function() {
	    	jQuery('body').removeClass('navigation-fullscreen-active');
	    }); 

		jQuery('.navigation-overlay .dropdown').hover(function() {
	    	jQuery('.sub-menu', this).slideToggle(300);
	    });  
});

