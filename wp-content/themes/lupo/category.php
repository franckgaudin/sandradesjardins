<?php
/*-----------------------------------------------------------------------------------*/
/* GET THEME HEADER
/*-----------------------------------------------------------------------------------*/

get_header();

/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>

<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
				<div id="posts" class="isotope-container">	
					<?php while ( have_posts() ) : the_post();
						global $more; $more = 0; global $excerpt_length; $excerpt_length = 20; ?>
						<div class="isotope-item col-md-4">
							<?php if(!get_post_format()) {
								get_template_part('post/'.'standard');
							} else {
								get_template_part('post/'.get_post_format());
							} ?>
						</div>
					<?php endwhile; ?>
				</div>

			</div>
		</div>
	</div>
</div>
<?php 

/*-----------------------------------------------------------------------------------*/
/* WORDPRESS RESET QUERY LOOP
/*-----------------------------------------------------------------------------------*/

wp_reset_query();

/*-----------------------------------------------------------------------------------*/
/* GET FOOTER
/*-----------------------------------------------------------------------------------*/

get_footer(); 

?>  