<?php get_header(); ?>

<div class="content-container">
    <div class="container">
        <div class="row">

            <div class="hero-container">
                <h1><?php the_title(); ?></h1>
            </div>

            <div class="col-md-8">
                <?php if (get_field('portfolio')) { ?>
                    <?php while(has_sub_field('portfolio')) { ?>
                        <div class="project-stack">
                            <img src="<?php the_sub_field('image'); ?>">
                        </div>
                    <?php } ?>
                <?php } ?>
		<?php
		    if(get_field('url_video_embed')){
			$video = get_field('url_video_embed');
			$shortcode = '[embed]'.$video.'[/embed]';
    			global $wp_embed;
    			echo $wp_embed->run_shortcode($shortcode);
		    } ?>
            </div>

            <div class="col-md-4">
                <?php if(have_posts()): while(have_posts()): the_post(); ?>
                    
                <!-- Begin Article -->
                <article class="project-sidebar">                           

                        <!-- Post Title -->
                        <div class="entry-header">
                            <?php if (get_the_title()) { ?>
                                <h1 class="heading">
                                    <?php the_title(); ?>
                                </h1>
                            <?php } ?>
                        </div>
                    
                        <!-- Post Content -->
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>

			

                         <?php if (get_theme_mod("social_share_buttons")) { ?>
                            <div class="portfolio-share">
                                <span>Share:</span> <?php moonbear_social_share(); ?>
                            </div>
                        <?php } ?>
                </article>

            <?php endwhile; endif; ?>

            </div>

        </div>
    </div>
</div>

<div class="footer-pagination">
    <span title="Previous project"><?php previous_post_link('%link', '<i class="fa fa-angle-left"></i> Précédent'); ?> </span>
    <span title="Back to portfolio"><a href="/">Retour au Portfolio</a></span>
    <span title="Next project"><?php next_post_link('%link', 'Suivant <i class="fa fa-angle-right"></i>'); ?> </span>
</div>

<?php get_footer(); ?>