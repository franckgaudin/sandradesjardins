<?php get_header();?>

<?php $sidebar = get_theme_mod('blog_sidebar');

if ($sidebar == "none" || $sidebar == "") { $span_size = "col-md-12"; }
if ($sidebar == "left" || $sidebar == "right") { $span_size = "col-md-9"; } ?>

<div class="content-container">
	<div class="container">
		<div class="row">

			<!-- LEFT SIDEBAR -->
			<?php if ($sidebar == "left") { ?>
				<?php if ( is_active_sidebar(get_theme_mod('select_blog_sidebar'))) { ?>
					<div class="col-md-3 widget-area widget-left">
						<?php dynamic_sidebar(get_theme_mod('select_blog_sidebar')); ?>
					</div>
				<?php } ?>
			<?php } ?>

			<div class="<?php echo esc_attr($span_size); ?>">	

				<div id="posts">	
					<?php while ( have_posts() ) : the_post();
						global $more; $more = 0; global $excerpt_length; $excerpt_length = 150; ?>
						<?php if(!get_post_format()) {
							get_template_part('post/'.'standard');
						} else {
							get_template_part('post/'.get_post_format());
						} ?>
					<?php endwhile; ?>

					<?php if (has_tag() || get_theme_mod("social_share_buttons")) { ?>
					<div class="tags-share">

	 					<div class="column">
	 					<?php if (has_tag()) { ?>
							Tagged In:<br>
							<?php the_tags( '', '', '' ); ?> 
						<?php } ?>
						</div>

						<div class="column right">
						<?php if (get_theme_mod("social_share_buttons")) { ?>
							Share:<br>
							<?php moonbear_social_share(); ?>
						<?php } ?>
						</div>
						
					</div>
					<?php } ?>

					<!-- Post Footer -->
					<?php if ( get_the_author_meta('description') ) { ?>
			   		<div class="author">
				   		<div id="authorarea">
				   			<?php echo get_avatar( get_the_author_meta( 'user_email' ), 150 ); ?>
							<h3><?php echo get_the_author(); ?></h3>
							<p>
								<?php the_author_meta( 'description' ); ?>
							</p>
						</div>
					</div>
					<?php } ?>

					<!-- Link Pages -->
					<div class="post-pagination">
						<?php moonbear_custom_wp_link_pages(); ?>
					</div>

					<div class="clearboth"></div>

		   		 	<div class="single-comments">

						<?php $comment_count = get_comment_count(); ?>
				 		<?php if ($comment_count['approved'] > 0) : ?>
					 		<?php if (comments_open()) { ?>

							<h4 class="small-title"><?php comments_number(); ?></h4>

							<?php } ?>
						<?php endif; ?>
							
						<?php 
						global $withcomments;
						$withcomments = true; 
						comments_template(); ?>

					</div>

				</div>
				<?php wp_reset_postdata(); ?>

			</div>

			<!-- RIGHT SIDEBAR -->
			<?php if ($sidebar == "right") { ?>
				<?php if ( is_active_sidebar(get_theme_mod('select_blog_sidebar'))) { ?>
					<div class="col-md-3 widget-area widget-right">
						<?php dynamic_sidebar(get_theme_mod('select_blog_sidebar')); ?>
					</div>
				<?php } ?>
			<?php } ?>


		</div>
	</div>
</div>
<?php get_footer(); ?>