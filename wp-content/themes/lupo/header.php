<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php if ( is_page_template('template-contact.php') ) { ?> onload="initialize()" <?php } ?>>

    <div class="preloader">
        <div class="preloader-content">
            <div class="pulse-preloader">
                <?php if (get_theme_mod('logo_image')) { ?>
                    <img src="<?php echo esc_url(get_theme_mod('logo_image')); ?>" alt=""/>
                <?php } else { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt=""/>
                <?php } ?>
            </div>
        </div>
    </div>

	<div class="page-container">
    
	<!-- NAVIGATION
	================================================== -->
	
	<!-- Mobile Navigation -->
	<?php do_action('mobile_navigation'); ?>
	
	<!-- Desktop Navigation -->
    <?php if (get_theme_mod('navigation_style')) { ?>
	   <?php get_template_part('framework/templates/navigation-'.get_theme_mod('navigation_style')); ?>
    <?php } else { ?>
        <?php get_template_part('framework/templates/navigation-standard'); ?>
    <?php } ?>