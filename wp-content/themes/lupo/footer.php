</div>

	<footer>

		<?php if (get_theme_mod('footer_logo') || get_theme_mod('footer_about_text')) { ?>
		<div class="footer-about">
			<?php if (get_theme_mod('footer_logo')) { ?>
				<?php if (get_theme_mod('logo_image')) { ?>
					<img src="<?php echo esc_url(get_theme_mod('logo_image')); ?>" alt=""/>
				<?php } else { ?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt=""/>
				<?php } ?>
			<?php } ?>
			<?php echo get_theme_mod("footer_about_text"); ?>
		</div>
		<?php } ?>
		<div class="social-buttons">
			<?php social_footer_buttons(); ?>
		</div>
		<div class="copyright">
			<?php echo get_theme_mod("footer_copyright"); ?>
		</div>
	</footer>

	<?php if (get_theme_mod('style_switcher')) { ?>
		<?php do_action('style_switcher'); ?>
	<?php } ?>
	
	<?php echo get_theme_mod('custom_js'); ?>
	<?php wp_footer(); ?>

	</body>
</html>