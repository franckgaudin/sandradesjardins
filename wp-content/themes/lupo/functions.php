<?php

/*-----------------------------------------------------------------------------------

    FUNCTIONS
    -----------------------------------------------------------------------------
	Editing this file will break the theme and possibly the universe.
	This file connects to our awesome framework.

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Paths
/*-----------------------------------------------------------------------------------*/

$moonbear_customizer_path = get_template_directory().'/framework/customizer';
$moonbear_tgm_path = get_template_directory().'/framework/tgm';
$moonbear_importer_path = get_template_directory().'/framework/importer';
$moonbear_hooks_path = get_template_directory().'/framework/hooks';

/*-----------------------------------------------------------------------------------*/
/*	Paths
/*-----------------------------------------------------------------------------------*/

require_once(get_template_directory()."/framework/functions.php");
require_once($moonbear_customizer_path . '/init.php');
if (!get_option("moonbear_installed")) {
	require_once($moonbear_importer_path . '/setup.php');
}
require_once($moonbear_hooks_path . '/hook_mobile_navigation.php');
require_once($moonbear_hooks_path . '/hook_navigation.php');
require_once($moonbear_hooks_path . '/hook_post.php');
require_once($moonbear_hooks_path . '/hook_single_portfolio_slider.php');
require_once($moonbear_hooks_path . '/hook_style_switcher.php');


/*-----------------------------------------------------------------------------------*/
/*	Remove for best perfomance
/*-----------------------------------------------------------------------------------*/
function remove_scripts_styles_footer() {
    //----- CSS
    wp_deregister_style('Open-Sans-css');

    //----- JS
    wp_deregister_script('twitter_feed');
}

add_action('wp_footer', 'remove_scripts_styles_footer');
?>