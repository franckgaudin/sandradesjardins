<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Standard
/*-----------------------------------------------------------------------------------*/
?>
<?php global $excerpt_length; ?>

<?php if (has_post_thumbnail( $post->ID ) ) { ?>
	<?php $featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php } ?>

	<!-- Begin Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php  $images = get_attached_media( 'image' ); ?>

		<div class="widget-slider">
			<div class="flexslider">
				<ul class="slides">
	
					<?php foreach($images as $image) { ?>
					<li>
						<div class="article-image">
							<a href="<?php the_permalink(); ?>" class="entry-image">

								<?php $image_url = wp_get_attachment_image_src($image->ID,'full'); ?>
								<img src="<?php echo esc_url($image_url[0]); ?>" />
							</a>
						</div>
					</li>
					<?php } ?>

				</ul>
			</div>
		</div>

	   	<div class="entry">

	   		<!-- Post Title -->
	   		<div class="entry-header">
		   		<?php if (is_single()) { ?>
			   		<h1 class="heading">
			   			<?php the_title(); ?>
			   		</h1>
			   	<?php } else { ?>
			   		<a href="<?php the_permalink(); ?>">
				   		<h1 class="heading">
				   			<?php the_title(); ?>
				   		</h1>
			   		</a>
			   	<?php } ?> 
			   	<div class="date">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
				</div>
				<div class="post-meta">
   					Posted <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?> by <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
   				</div>
   			</div>

	   		<!-- Post Content -->
			<div class="entry-content">
		   		<?php if (is_single()) { ?>
			   		<?php the_content(); ?>
			   	<?php } else { ?>
				   	<?php the_excerpt(); ?>
			   	<?php } ?> 
	   		</div>

	   		<?php if (!is_single()) { ?>
	   			<div class="read-more-wrapper"><a href="<?php the_permalink(); ?>" class="read-more">Continue Reading</a></div>
	   		<?php } ?>
	   		
		</div>	
	</article>