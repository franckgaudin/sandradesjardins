<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Standard
/*-----------------------------------------------------------------------------------*/
?>
<?php global $excerpt_length; ?>

<?php if (has_post_thumbnail( $post->ID ) ) { ?>
	<?php $featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php } ?>

	<!-- Begin Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( has_post_thumbnail() ) {  ?>
		<a href="<?php the_permalink(); ?>" class="article-image portfolio-entry">		
			<?php if (!is_single()) { ?>	
			<div class="overlay">
				<div class="overlay-center">
					<?php if (get_theme_mod('light_logo_image')) { ?>
						<img class="overlay-logo" src="<?php echo esc_url(get_theme_mod('light_logo_image')); ?>" alt=""/>
					<?php } else { ?>
						<img class="overlay-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_white.png" alt=""/>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
			<?php the_post_thumbnail('full');  ?>
		</a>
		<?php } ?>

	   	<div class="entry">

	   		<!-- Post Title -->
	   		<div class="entry-header">
		   		<?php if (is_single()) { ?>
			   		<h1 class="heading">
			   			<?php the_title(); ?>
			   		</h1>
			   	<?php } else { ?>
			   		<a href="<?php the_permalink(); ?>">
				   		<h1 class="heading">
				   			<?php the_title(); ?>
				   		</h1>
			   		</a>
			   	<?php } ?> 
			   	<div class="date">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
				</div>
				<div class="post-meta">
   					Posted <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?> by <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
   				</div>
   			</div>

	   		<!-- Post Content -->
			<div class="entry-content">
		   		<?php if (is_single()) { ?>
			   		<?php the_content(); ?>
			   	<?php } else { ?>
				   	<?php the_excerpt(); ?>
			   	<?php } ?> 
	   		</div>

	   		<?php if (!is_single()) { ?>
	   			<div class="read-more-wrapper"><a href="<?php the_permalink(); ?>" class="read-more">Continue Reading</a></div>
	   		<?php } ?>
	   		
		</div>	
	</article>