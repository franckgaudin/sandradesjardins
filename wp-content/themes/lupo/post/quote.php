<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Standard
/*-----------------------------------------------------------------------------------*/
?>
<?php global $excerpt_length; ?>

<?php if (has_post_thumbnail( $post->ID ) ) { ?>
	<?php $featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php } ?>

<?php

$content = trim(get_the_content());

// Make sure content isn't empty
if (!$content == "") {

   // Take each new line and put into an array (for multiple quotes)
   preg_match("#<blockquote>Current Value: </td><td>(.+?)</blockquote>#", $content, $output);

   $pattern = "/<blockquote>(.*?)<\/blockquote>/";
   preg_match($pattern, $content, $matches);

   // Get the first quote and do something with it
   $first_quote = $matches[1];
}

?>

	<!-- Begin Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( has_post_thumbnail() ) {  ?>
		<a href="<?php the_permalink(); ?>" class="article-image">		
			<?php the_post_thumbnail('full');  ?>
		</a>
		<?php } ?>

		<div class="article-quote">
	   		<i class="fa fa-quote-left"></i> &nbsp; <?php echo esc_html($first_quote); ?></a>
	   	</div>

	   	<div class="entry">

	   		<!-- Post Title -->
	   		<div class="entry-header">
		   		<?php if (is_single()) { ?>
			   		<h1 class="heading">
			   			<?php the_title(); ?>
			   		</h1>
			   	<?php } else { ?>
			   		<a href="<?php the_permalink(); ?>">
				   		<h1 class="heading">
				   			<?php the_title(); ?>
				   		</h1>
			   		</a>
			   	<?php } ?> 
			   	<div class="date">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
				</div>
				<div class="post-meta">
   					Posted <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?> by <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
   				</div>
   			</div>

	   		<!-- Post Content -->
			<div class="entry-content">
		   		<?php if (is_single()) { ?>
			   		<?php the_content(); ?>
			   	<?php } else { ?>
				   	<?php the_excerpt(); ?>
			   	<?php } ?> 
	   		</div>

	   		<?php if (!is_single()) { ?>
	   			<div class="read-more-wrapper"><a href="<?php the_permalink(); ?>" class="read-more">Continue Reading</a></div>
	   		<?php } ?>
	   		
		</div>	
	</article>