<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Standard
/*-----------------------------------------------------------------------------------*/
?>
<?php global $excerpt_length; ?>

<?php if (has_post_thumbnail( $post->ID ) ) { ?>
	<?php $featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php } ?>

<?php 

	$content = get_the_content();
	preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $content, $output);
	$parsedUrl  = parse_url($output);
	$embed      = $parsedUrl['query'];
	parse_str($embed, $out);
	$embedUrl   = $output[1];

?>
	<!-- Begin Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="article-video">
			<?php if (!is_single()) { ?>
				<iframe width="350" height="197" src="http://www.youtube.com/embed/<?php echo esc_url($embedUrl); ?>" frameborder="0" allowfullscreen></iframe>
			<?php } else { ?>
				<iframe width="100%" height="467" src="http://www.youtube.com/embed/<?php echo esc_url($embedUrl); ?>" frameborder="0" allowfullscreen></iframe>
			<?php } ?>
		</div>


	   	<div class="entry">

	   		<!-- Post Title -->
	   		<div class="entry-header">
		   		<?php if (is_single()) { ?>
			   		<h1 class="heading">
			   			<?php the_title(); ?>
			   		</h1>
			   	<?php } else { ?>
			   		<a href="<?php the_permalink(); ?>">
				   		<h1 class="heading">
				   			<?php the_title(); ?>
				   		</h1>
			   		</a>
			   	<?php } ?> 
			   	<div class="date">
					<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
				</div>
				<div class="post-meta">
   					Posted <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?> by <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
   				</div>
   			</div>

	   		<!-- Post Content -->
			<div class="entry-content">
		   		<?php if (is_single()) { ?>
			   		<?php the_content(); ?>
			   	<?php } else { ?>
				   	<?php the_excerpt(); ?>
			   	<?php } ?> 
	   		</div>

	   		<?php if (!is_single()) { ?>
	   			<div class="read-more-wrapper"><a href="<?php the_permalink(); ?>" class="read-more">Continue Reading</a></div>
	   		<?php } ?>
	   		
		</div>	
	</article>