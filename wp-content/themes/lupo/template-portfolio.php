<?php
/*
Template Name: Portfolio
*/
?>

<?php get_header(); ?>

<?php

/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>

<?php
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
query_posts(array('post_type'=>'portfolio', 'paged' => $paged, 'posts_per_page' => get_theme_mod('portfolio_per_page')));
?>

<?php if (get_theme_mod('portfolio_style')) { ?>
   <?php get_template_part('framework/templates/portfolio-'.get_theme_mod('portfolio_style')); ?>
<?php } else { ?>
    <?php get_template_part('framework/templates/portfolio-standard'); ?>
<?php } ?>

<!-- Load More -->
<div class="isotope-loadmore"><?php posts_nav_link(' &#183; ', 'Older', '<i class="fa fa-plus"></i> Load more'); ?></div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>