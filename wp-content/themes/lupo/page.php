<?php get_header(); ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* SET CONTENT WIDTH
/*-----------------------------------------------------------------------------------*/

$sidebar = get_field('sidebar_position');

if ($sidebar == "none" || $sidebar == "") { $span_size = "col-md-12"; }
if ($sidebar == "left" || $sidebar == "right") { $span_size = "col-md-8"; }


/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>

<div class="content-container">
	<div class="container">
		<div class="row">

			<?php if (get_field('hero_text')) { ?>
				<div class="hero-container">
					<?php the_field("hero_text"); ?>
				</div>
			<?php } ?>
		
			<!-- LEFT SIDEBAR -->
			<?php if ($sidebar == "left") { ?>
				<?php if ( is_active_sidebar(get_field("sidebars"))) { ?>
				<div class="col-md-4 widget-area widget-left">
					<?php dynamic_sidebar(get_field("sidebars")); ?>
				</div>
				<?php } ?>
			<?php } ?>
			
			<!-- MAIN CONTENT -->
			<div class="<?php echo esc_attr($span_size); ?>">
				<?php while (have_posts()) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
						<?php the_content(); ?>		
						</div>	
					</div>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>

				<div class="single-comments">

					<?php $comment_count = get_comment_count(); ?>
			 		<?php if ($comment_count['approved'] > 0) : ?>
				 		<?php if (comments_open()) { ?>

						<h4 class="small-title"><?php comments_number(); ?></h4>

						<?php } ?>
					<?php endif; ?>
						
					<?php 
					global $withcomments;
					$withcomments = true; 
					comments_template(); ?>

					
				</div>

			</div>
			
			<?php if ($sidebar == "right") { ?>
				<?php if ( is_active_sidebar(get_field("sidebars"))) { ?>
					<div class="col-md-4 widget-area widget-right">
						<?php dynamic_sidebar(get_field("sidebars")); ?>
					</div>
				<?php } ?>
			<?php } ?>
			
		</div>
	</div>
</div>

<?php get_footer(); ?>