<?php
/*
Template Name: Blog
*/
?>

<?php get_header();?>

<?php
/*-----------------------------------------------------------------------------------*/
/* SET CONTENT WIDTH
/*-----------------------------------------------------------------------------------*/

$sidebar = get_field('sidebar_position');

if ($sidebar == "none" || $sidebar == "") { $span_size = "col-md-8 col-md-offset-2"; }
if ($sidebar == "left" || $sidebar == "right") { $span_size = "col-md-7 col-md-offset-1"; }

/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>

<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<?php if (get_field('hero_text')) { ?>
					<div class="hero-container">
						<?php the_field("hero_text"); ?>
					</div>
				<?php } ?>
	
				<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts(array('post_type'=>'post', 'paged' => $paged, 'posts_per_page' => get_option('posts_per_page')));
				?>
				
				<div id="posts" class="isotope-container">	
					<?php while ( have_posts() ) : the_post();
						global $more; $more = 0; global $excerpt_length; $excerpt_length = 20; ?>
						<div class="isotope-item col-md-4">
							<?php if(!get_post_format()) {
								get_template_part('post/'.'standard');
							} else {
								get_template_part('post/'.get_post_format());
							} ?>
						</div>
					<?php endwhile; ?>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- Load More -->
<div class="isotope-loadmore"><?php posts_nav_link(' &#183; ', 'Older', '<i class="fa fa-plus"></i> Load more'); ?></div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>  