<?php
/*-----------------------------------------------------------------------------------

    CUSTOMIZER GENERAL
    -----------------------------------------------------------------------------

    This file adds the general settings into the customizer.
	Editing this file will break the theme and possibly the universe.


    TABLE OF CONTENTS
    -----------------------------------------------------------------------------

    1.0 PRESETS
    ---------------------------

    2.0 GENERAL PANEL
    ---------------------------
        1.1 Theme Colour
        1.2 Layout Width
        1.3 Logo
        1.4 Layout
        1.5 Background
        1.6 Buttons
        1.7 Sitewide Alert
        1.8 Preloader

-----------------------------------------------------------------------------------*/

		Kirki::add_config( 'aspen', array(
		    'option_type' => 'theme_mod',
		    'capability'  => 'edit_theme_options',
		) );

	/*-----------------------------------------------------------------------------------*/
	/*	Branding
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'branding_section', array(
	    'title'          => 'Branding',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 1,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

		/*-----------------------------------------------------------------------------------*/
		/*	Main Logo
		/*-----------------------------------------------------------------------------------*/

		Kirki::add_field( 'aspen', array(
		    'type'        => 'image',
		    'settings'     => 'logo_image',
		    'label'       => 'Main Logo',
		    'description'       => 'This is the main logo used on the website',
		    'section'     => 'branding_section',
		    'priority'    => 1,
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	Light Logo
		/*-----------------------------------------------------------------------------------*/

		Kirki::add_field( 'aspen', array(
		    'type'        => 'image',
		    'settings'     => 'light_logo_image',
		    'label'       => 'Light Logo',
		    'description'       => 'This is the light logo used on dark backgrounds',
		    'section'     => 'branding_section',
		    'priority'    => 2,
		) );

	/*-----------------------------------------------------------------------------------*/
	/*	Header
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'header_section', array(
	    'title'          => 'Header',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 1,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

		/*-----------------------------------------------------------------------------------*/
		/*	Navigation Style
		/*-----------------------------------------------------------------------------------*/


		Kirki::add_field( 'aspen', array(
		    'type'        => 'select',
		    'settings'    => 'navigation_style',
		    'label'       => 'Navigation Style',
		    'description' => 'Select a navigation style',
		    'section'     => 'header_section',
		    'priority'    => 1,
		    'default'		=> 'standard',
		    'choices'     => array(
		        'standard' 	=> "Standard",
		        'side' 	=> "Side",
		        'fullscreen' 	=> "Fullscreen Overlay",
		    )
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	Sticky header
		/*-----------------------------------------------------------------------------------*/

		Kirki::add_field( 'aspen', array(
		    'type'        => 'switch',
		    'settings'     => 'sticky_header',
		    'description' 	=> 'The sticky header is the fixed compact menu displayed when you scroll down the page',
		    'label'       => __( 'Sticky Header', 'kirki' ),
		    'section'     => 'header_section',
		    'default'     => '1',
		    'priority'    => 2,
		) );

	/*-----------------------------------------------------------------------------------*/
	/*	Blog (section)
	/*-----------------------------------------------------------------------------------*/

		Kirki::add_section( 'blog_section', array(
		    'title'          => 'Blog',
		    'description'    => 'Blog Settings',
		    'panel'          => '', // Not typically needed.
		    'priority'       => 12,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '', // Rarely needed.
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	Single Post Sidebar (section)
		/*-----------------------------------------------------------------------------------*/

	    Kirki::add_field( 'aspen', array(
		    'type'			=> 'select',
		    'settings'     	=> 'blog_sidebar',
		    'label' 		=> 	'Blog sidebar',
		    'description' 	=> 'Sidebar setting for auto-generated pages (single post, categories, etc)',
		    'section'     	=> 'blog_section',
		    'default'     	=> 'none',
		    'priority'    	=> 2,
		    'choices'     	=> array(
		        'none' => 'None',
		        'left' => 'Left',
		        'right' => 'Right',
		    ),
		) );

	    /*-----------------------------------------------------------------------------------*/
		/*	Single Post Sidebar (section)
		/*-----------------------------------------------------------------------------------*/

		Kirki::add_field( 'aspen', array(
		    'type'        => 'select',
		    'settings'     => 'select_blog_sidebar',
		    'label'       => 'Sidebar',
		    'description' => 'Sidebar setting for auto-generated pages (single post, categories, etc)',
		    'help'        => 'You can go into Appearance->Widgets to add widgets to your sidebars',
		    'section'     => 'blog_section',
		    'default'     => 'widget-area-1',
		    'priority'    => 3,
		    'choices'     => array(
		        'widget-area-1' => 'Widget Area 1',
		        'widget-area-2' => 'Widget Area 2',
		        'widget-area-3' => 'Widget Area 3',
		        'widget-area-4' => 'Widget Area 4',
		        'widget-area-5' => 'Widget Area 5',
		        'widget-area-6' => 'Widget Area 6'
		    ),
		) );


	/*-----------------------------------------------------------------------------------*/
	/*	Footer
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'footer_section', array(
	    'title'          => 'Footer',
	    'description'    => 'Blog Settings',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 3,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'textarea',
	    'settings'     => 'footer_copyright',
	    'label'       => 'Copyright text',
	    'description' => 'Add your custom copyright text',
	    'section'     => 'footer_section',
	    'priority'    => 1,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'select',
	    'settings'    => 'social_footer_buttons',
	    'label'       => 'Social Footer Buttons',
	    'description' => 'Add social footer buttons to the posts',
	    'help'        => 'Go into the Social section to assign links to your social pages',
	    'section'     => 'footer_section',
	    'priority'    => 2,
	    'choices'     => array(
	        'facebook' 	=> "Facebook",
	        'twitter' 	=> "Twitter",
	        'google' 	=> "Google+",
	        'instagram' => "Instagram",
	        'linkedin' 	=> "LinkedIn",
	        'pinterest' => "Pinterest",
	        'youtube' 	=> "Youtube",
	        'vine' 		=> "Vine",
	        'vimeo' 	=> "Vimeo",
	        'soundcloud' 	=> "Soundcloud",
	    ),
	    'multiple'    => 9,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'switch',
	    'settings'     => 'footer_logo',
	    'label'       => __( 'Footer Logo', 'kirki' ),
	    'section'     => 'footer_section',
	    'description' => 'Display your logo in the footer. You can upload the logo in the Branding section.',
	    'default'     => '1',
	    'priority'    => 3,
	) );


	Kirki::add_field( 'aspen', array(
	    'type'        => 'editor',
	    'settings'    => 'footer_about_text',
	    'label'       => __( 'Editor Control', 'kirki-demo' ),
	    'description' => 'Add some custom content in the footer',
	    'section'     => 'footer_section',
	    'default'     => '',
	    'sanitize_callback' => array( 'Kirki_Sanitize_Values', 'unfiltered' ),
	    'priority'    => 5,
	) );

	/*-----------------------------------------------------------------------------------*/
	/*	Portfolio
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'portfolio_section', array(
	    'title'          => 'Portfolio',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 13,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'select',
	    'settings'    => 'portfolio_style',
	    'label'       => 'Portfolio Style',
	    'description' => 'Select a portfolio style',
	    'section'     => 'portfolio_section',
	    'priority'    => 1,
	    'choices'     => array(
	        'standard' 	=> "Standard",
	        'fullscreen' 	=> "Fullscreen",
	    ),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'select',
	    'settings'    => 'grid_size',
	    'label'       => 'Grid Size',
	    'description' => 'Portfolio Grid Size',
	    'section'     => 'portfolio_section',
	    'priority'    => 1,
	    'choices'     => array(
	        'portfolio-2col' 	=> "Two columns",
	        'portfolio-3col' 	=> "Three columns",
	        'portfolio-4col' 	=> "Four columns",
	    ),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'portfolio_per_page',
	    'label'       => 'Portfolio posts per page',
	    'description' => 'Select how many portfolio posts to display per page',
	    'section'     => 'portfolio_section',
	    'default'     => '11',
	    'priority'    => 2,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     		=> 'slider',
	    'settings'     => 'portfolio_grid_spacing',
	    'label'       => 'Portfolio Grid Spacing',
	    'description' => 'Amount of space in between each image',
	    'section'     => 'portfolio_section',
	    'default'     	=> 0,
	    'priority'    	=> 4,
	    'choices'     	=> array(
	        'min'  => 0,
	        'max'  => 50,
	        'step' => 1,
	    ),
	    'output'   => array( array(
		        'element'  => '.portfolio-entry',
		        'property' => 'border-width',
		        'units'    => 'px',
			)
		),
		'transport' => 'postMessage',
	    'js_vars'   => array( array(
	            'element'  => '.portfolio-entry',
	            'function' => 'css',
		        'property' => 'border-width',
		        'units'    => 'px',
	        )
	    )
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'switch',
	    'settings'     => 'portfolio_filter',
	    'label'       => __( 'Enable the portfolio filter', 'kirki' ),
	    'section'     => 'portfolio_section',
	    'priority'    => 5,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'select',
	    'settings'    => 'portfolio_overlay_style',
	    'label'       => 'Portfolio Overlay Style',
	    'section'     => 'portfolio_section',
	    'priority'    => 6,
	    'choices'     => array(
	        'logo_title' 	=> "Logo + Title",
	        'logo' 	=> "Logo",
	        'title' 	=> "Title",
	    ),
	) );

	Kirki::add_field( '', array(
	    'type'        => 'color-alpha',
	    'settings'    => 'portfolio_overlay_colour',
	    'label'       => 'Portfolio Overlay Colour',
	    'section'     => 'portfolio_section',
	    'default'     => 'rgba(0,0,0,0.7)',
	    'priority'    => 7,
	    'output'   => array( array(
		        'element'  => '.image-overlay .overlay',
		        'property' => 'background-color',
			)
		),
		'transport' => 'postMessage',
	    'js_vars'   => array( array(
	            'element'  => '.image-overlay .overlay',
	            'function' => 'css',
		        'property' => 'background-color',
	        )
	    )
	) );

	/*-----------------------------------------------------------------------------------*/
	/*	Social Profiles
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'social_section', array(
	    'title'          => 'Social Settings',
	    'description'    => 'Social Settings',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 14,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'select',
	    'settings'    => 'social_share_buttons',
	    'label'       => 'Social Share Buttons',
	    'description' => 'Add social share buttons to the posts',
	    'help'        => 'This is some extra help. You can use this to add some additional instructions for users. The main description should go in the "description" of the field, this is only to be used for help tips.',
	    'section'     => 'social_section',
	    'priority'    => 1,
	    'choices'     => array(
	        'facebook' 	=> "Facebook",
	        'twitter' 	=> "Twitter",
	        'google' 	=> "Google+",
	        'pinterest' => "Pinterest",
	        'linkedin' 	=> "LinkedIn",
	    ),
	    'multiple'    => 5,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'facebook_profile',
	    'label'       => 'Facebook profile',
	    'description' => 'Enter the link to your facebook profile',
	    'section'     => 'social_section',
	    'default'     => 'http://', 'kirki',
	    'priority'    => 2,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'twitter_profile',
	    'label'       => 'Twitter profile',
	    'description' => 'Enter the link to your twitter profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 3,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'google_profile',
	    'label'       => 'Google Plus profile',
	    'description' => 'Enter the link to your google plus profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 4,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'instagram_profile',
	    'label'       => 'Instagram profile',
	    'description' => 'Enter the link to your instagram profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 5,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'linkedin_profile',
	    'label'       => 'LinkedIn profile',
	    'description' => 'Enter the link to your LinkedIn profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 6,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'pinterest_profile',
	    'label'       => 'Pinterest profile',
	    'description' => 'Enter the link to your pinterest profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 7,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'youtube_profile',
	    'label'       => 'Youtube profile',
	    'description' => 'Enter the link to your youtube profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 8,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'vine_profile',
	    'label'       => 'Vine profile',
	    'description' => 'Enter the link to your vine profile',
	    'section'     => 'social_section',
	    'default'     => 'http://', 'kirki',
	    'priority'    => 9,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'vimeo_profile',
	    'label'       => 'Vimeo profile', 'kirki',
	    'description' => 'Enter the link to your vimeo profile',
	    'section'     => 'social_section',
	    'default'     => 'http://',
	    'priority'    => 10,
	) );

	/*-----------------------------------------------------------------------------------*/
	/*	Twitter
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'twitter_section', array(
	    'title'          => 'Twitter API',
	    'description'    => 'Twitter Settings',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 14,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'twitter_consumer_key',
	    'label'       => 'Twitter Consumer Key',
	    'description' => 'Enter your twitter consumer key',
	    'section'     => 'twitter_section',
	    'priority'    => 2,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'twitter_consumer_secret',
	    'label'       => 'Twitter Consumer Secret',
	    'description' => 'Enter your twitter consumer secret',
	    'section'     => 'twitter_section',
	    'priority'    => 3,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'twitter_access_token',
	    'label'       => 'Twitter Access Token',
	    'description' => 'Enter your twitter access token',
	    'section'     => 'twitter_section',
	    'priority'    => 4,
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'text',
	    'settings'     => 'twitter_access_token_secret',
	    'label'       => 'Twitter Access Token Secret',
	    'description' => 'Enter your twitter access token secret',
	    'section'     => 'twitter_section',
	    'priority'    => 4,
	) );

	/*-----------------------------------------------------------------------------------*/
	/*	Typography
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'typography_section', array(
		    'title'          => 'Typography',
		    'description'    => 'Typography Settings',
		    'panel'          => '', // Not typically needed.
		    'priority'       => 15,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     => 'select',
	    'settings'  => 'body_font',
	    'label'    => 'Body Font',
	    'section'  => 'typography_section',
	    'priority' => 1,
	    'choices'  => Kirki_Fonts::get_font_choices(),
	     'output'   => array( array(
		        'element'  => 'body',
		        'property' => 'font-family',
		   )),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     => 'select',
	    'settings'  => 'heading_font',
	    'label'    => 'Heading Font',
	    'section'  => 'typography_section',
	    'priority' => 2,
	    'choices'  => Kirki_Fonts::get_font_choices(),
	    'output'   => array( array(
		        'element'  => 'h1, h2, h3, h4, h5, h6',
		        'property' => 'font-family',
		)),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     => 'select',
	    'settings'  => 'navigation_font',
	    'label'    => 'Navigation Font',
	    'section'  => 'typography_section',
	    'priority' => 3,
	    'choices'  => Kirki_Fonts::get_font_choices(),
	    'output'   => array( array(
		        'element'  => '.navigation li a',
		        'property' => 'font-family',
		)),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     => 'select',
	    'settings'  => 'hero_font',
	    'label'    => 'Hero Text Font',
	    'section'  => 'typography_section',
	    'default'  => 'Oswald',
	    'priority' => 3,
	    'choices'  => Kirki_Fonts::get_font_choices(),
	    'output'   => array( array(
		        'element'  => '.hero-container h1',
		        'property' => 'font-family',
		)),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     => 'select',
	    'settings'  => 'hero_text_style',
	    'label'    => 'Hero Text Style',
	    'section'  => 'typography_section',
	    'default'  => 'uppercase',
	    'priority' => 4,
	    'choices'     => array(
	        'none' 	=> "Standard",
	        'uppercase' 	=> "Uppercase",
	    ),
	    'output'   => array( array(
		        'element'  => '.hero-container h1',
		        'property' => 'text-transform',
		)),
		'transport' => 'postMessage',
		'js_vars'   => array( array(
	            'element'  => '.hero-container h1',
	            'function' => 'css',
		        'property' => 'text-transform',
		        'units'    => '',
	        )
	    )
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     		=> 'slider',
	    'settings'     	=> 'body_font_size',
	    'label' 		=> 	'Body Font Size',
	    'description' 	=> 'Change the body font size',
	    'section'     	=> 'typography_section',
	    'default'     	=> 14,
	    'priority'    	=> 5,
	    'choices'     	=> array(
	        'min'  => 8,
	        'max'  => 26,
	        'step' => 1,
	    ),
	    'output'   => array( array(
		        'element'  => 'p',
		        'property' => 'font-size',
		        'units'    => 'px',
			)
		),
		'transport' => 'postMessage',
	    'js_vars'   => array( array(
	            'element'  => 'p',
	            'function' => 'css',
		        'property' => 'font-size',
		        'units'    => 'px',
	        )
	    )
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     		=> 'slider',
	    'settings'     	=> 'navigation_font_size',
	    'label' 		=> 	'Navigation Font Size',
	    'description' 	=> 'Change the navigation font size',
	    'section'     	=> 'typography_section',
	    'default'     	=> 11,
	    'priority'    	=> 6,
	    'choices'     	=> array(
	        'min'  => 8,
	        'max'  => 26,
	        'step' => 1,
	    ),
	    'output'   => array( array(
		        'element'  => '.navigation > li > a',
		        'property' => 'font-size',
		        'units'    => 'px',
			)
		),
		'transport' => 'postMessage',
	    'js_vars'   => array( array(
	            'element'  => '.navigation > li > a',
	            'function' => 'css',
		        'property' => 'font-size',
		        'units'    => 'px',
	        )
	    )
	) );

	Kirki::add_field( 'aspen', array(
	    'type'     		=> 'slider',
	    'settings'     	=> 'body_l',
	    'label' 		=> 	'Navigation Font Size',
	    'description' 	=> 'Change the navigation font size',
	    'section'     	=> 'typography_section',
	    'default'     	=> 11,
	    'priority'    	=> 7,
	    'choices'     	=> array(
	        'min'  => 8,
	        'max'  => 26,
	        'step' => 1,
	    ),
	    'output'   => array( array(
		        'element'  => '.navigation > li > a',
		        'property' => 'font-size',
		        'units'    => 'px',
			)
		),
		'transport' => 'postMessage',
	    'js_vars'   => array( array(
	            'element'  => '.navigation > li > a',
	            'function' => 'css',
		        'property' => 'font-size',
		        'units'    => 'px',
	        )
	    )
	) );

	/*-----------------------------------------------------------------------------------*/
	/*	Misc
	/*-----------------------------------------------------------------------------------*/

	Kirki::add_section( 'misc_section', array(
	    'title'          => 'Misc',
	    'panel'          => '', // Not typically needed.
	    'priority'       => 16,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '', // Rarely needed.
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'code',
	    'settings'    => 'custom_css',
	    'label'       => 'Custom CSS',
	    'section'     => 'misc_section',
	    'default'     => 'CSS code in here',
	    'priority'    => 1,
	    'choices'     => array(
	        'language' => 'css',
	        'theme'    => 'monokai',
	        'height'   => 200,
	    ),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'code',
	    'settings'    => 'custom_javascript',
	    'label'       => 'Javasript',
	    'section'     => 'misc_section',
	    'description' => 'Place your tracking codes or custom javascript in here inside <script> tags',
	    'default'     => '<script></script>',
	    'priority'    => 2,
	    'choices'     => array(
	        'language' => 'javascript',
	        'theme'    => 'monokai',
	        'height'   => 200,
	    ),
	) );

	Kirki::add_field( 'aspen', array(
	    'type'        => 'switch',
	    'settings'     => 'style_switcher',
	    'label'       => __( 'Enable the demo switcher', 'kirki' ),
	    'section'     => 'misc_section',
	    'default'     => '0',
	    'priority'    => 3,
	) );

?>