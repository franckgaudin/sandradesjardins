<?php
/*-----------------------------------------------------------------------------------*/
/*	Section Description
/*-----------------------------------------------------------------------------------*/

function ctx_customizer_register( $wp_customize ) {

	/*-----------------------------------------------------------------------------------*/
	/*	Sidebar
	/*-----------------------------------------------------------------------------------*/

	class Sidebar_Control extends WP_Customize_Control {
		public function render_content() { ?>
			<label>
				<div class="customize-control-title"><?php echo esc_html( $this->label ); ?></div>
				<span class="description customize-control-description"><?php echo esc_html($this->description); ?></span>

				<select <?php $this->link(); ?>>					
					<?php
					$field['choices'] = array();
					$field['choices']['widget-area-1'] = 'Widget Area 1';
					$field['choices']['widget-area-2'] = 'Widget Area 2';
					$field['choices']['widget-area-3'] = 'Widget Area 3';
					$field['choices']['widget-area-4'] = 'Widget Area 4';
					$field['choices']['widget-area-5'] = 'Widget Area 5';
					$field['choices']['widget-area-6'] = 'Widget Area 6';

					for($i = 0; $i < get_theme_mod('number_of_sidebars'); ++$i) {
						$s_name = "Sidebar_".$i;
						$field['choices'][ $s_name ] = $s_name;
					};
					foreach($field["choices"] as $fields) {
						echo "<option name=".$this->id." name=".$this->id.$fields." value='".$fields."'>".$fields."</option>";
					}
					?>
				</select>
			</label>
	    <?php
		}
	}


	/*-----------------------------------------------------------------------------------*/
	/*	Section Heading
	/*-----------------------------------------------------------------------------------*/

	class Section_Heading extends WP_Customize_Control {
	    public $type = 'sectionheading';
	 
	    public function render_content() { ?>
	        <div class="customize-section-space"></div>
	        <div class="customize-section-heading customize-control-title"><?php echo esc_html( $this->label ); ?></div>
	        <?php
	    }
	}

	/*-----------------------------------------------------------------------------------*/
	/*	Section Description
	/*-----------------------------------------------------------------------------------*/

	class Section_Description extends WP_Customize_Control {
	    public $type = 'sectiondescription';
	 
	    public function render_content() {
	        ?>
	        <p class="description"><?php echo esc_html( $this->label ); ?></p>
	        <?php
	    }
	}
	
	/*-----------------------------------------------------------------------------------*/
	/*	Add options and settings
	/*-----------------------------------------------------------------------------------*/
	
}

add_action( 'customize_register', 'ctx_customizer_register' );


include("customizer.php");

?>