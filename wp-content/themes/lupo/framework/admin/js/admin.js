jQuery(window).load(function() {
    jQuery( "#accordion-section-branding_section > .accordion-section-title" ).prepend( "<i class='fa fa-image'></i>");
    jQuery( "#accordion-section-header_section > .accordion-section-title" ).prepend( "<i class='fa fa-calendar-o'></i>");
    jQuery( "#accordion-section-footer_section > .accordion-section-title" ).prepend( "<i class='fa fa-hdd-o'></i>");
    jQuery( "#accordion-section-blog_section > .accordion-section-title" ).prepend( "<i class='fa fa-pencil'></i>");
    jQuery( "#accordion-section-portfolio_section > .accordion-section-title" ).prepend( "<i class='fa fa-image'></i>");
    jQuery( "#accordion-section-social_section > .accordion-section-title" ).prepend( "<i class='fa fa-users'></i>");
    jQuery( "#accordion-section-twitter_section > .accordion-section-title" ).prepend( "<i class='fa fa-twitter'></i>");
    jQuery( "#accordion-section-typography_section > .accordion-section-title" ).prepend( "<i class='fa fa-font'></i>");
    jQuery( "#accordion-section-misc_section > .accordion-section-title" ).prepend( "<i class='fa fa-heart-o'></i>");
    jQuery( "#accordion-panel-nav_menus > .accordion-section-title" ).prepend( "<i class='fa fa-align-justify'></i>");
    jQuery( "#accordion-section-google_maps_section > .accordion-section-title" ).prepend( "<i class='fa fa-globe'></i>");
    jQuery( "#accordion-section-title_tagline > .accordion-section-title" ).prepend( "<i class='fa fa-file-o'></i>");
    jQuery( "#accordion-section-nav > .accordion-section-title" ).prepend( "<i class='fa fa-bars'></i>");
    jQuery( "#accordion-panel-widgets > .accordion-section-title" ).prepend( "<i class='fa fa-columns'></i>");
    jQuery( "#accordion-section-static_front_page > .accordion-section-title" ).prepend( "<i class='fa fa-home'></i>");
});

/*-----------------------------------------------------------------------------------*/
/* Demo Setup
/*-----------------------------------------------------------------------------------*/


jQuery(document).ready(function() {
    "use strict";
    jQuery('.start-import').click(function(){
        var data_import = jQuery(this).attr("data-import");

        var this_button = jQuery(this);

        jQuery(this_button).text("Importing... Please wait.");
        jQuery(".start-import").attr('disabled','disabled');

        jQuery(this_button).removeClass('button-primary');
         jQuery(this_button).addClass('button-secondary');

        var moonbear_plugins = {
            'action': 'moonbear_plugins'       
        };

        var moonbear_import = {
            'action': 'moonbear_import_'+data_import   
        };

       // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, moonbear_plugins, function(response) {

            jQuery.post(ajaxurl, moonbear_import, function(response) {
                jQuery(this_button).text("Import Complete");
        
            });

        });

    });

    jQuery('#skip-setup').click(function(){
        jQuery(".updated-setup").remove();

        var moonbear_skip_setup = {
            'action': 'moonbear_skip_setup'       
        };
        jQuery.post(ajaxurl, moonbear_skip_setup, function(response) {});
    });
});

/*-----------------------------------------------------------------------------------*/
/* Customizer Preloader
/*-----------------------------------------------------------------------------------*/

jQuery(window).load(function(){
    jQuery( ".customizer-preset li" ).click(function() { 
        jQuery(this).addClass("active");
    });
});

/*-----------------------------------------------------------------------------------*/
/* Customizer Preloader
/*-----------------------------------------------------------------------------------*/

jQuery(window).load(function(){
    "use strict";
     jQuery('.customizer-preloader-container').css("display", "none");
     jQuery('.customizer-preloader-container').css("opacity", "0");
});