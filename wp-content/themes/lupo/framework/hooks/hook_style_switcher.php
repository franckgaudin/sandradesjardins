<?php
/*-----------------------------------------------------------------------------------*/
/*	Style Switcher
/*-----------------------------------------------------------------------------------*/

add_action('style_switcher', 'themize_style_switcher'); 

function themize_style_switcher() { ?>

	<div class="live-previews-container">
		<div class="live-previews-button">
			<i class="fa fa-cog"></i> One Click Demos
		</div>

		<div class="live-previews-screenshots">
			<div class="live-previews-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_white.png" alt="">
				<p>You can easily install and mix and match any style in one simple click</p>
			</div>


			<div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Default</h4>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot1.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

			<div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo2" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #2</h4>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot2.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

			<div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo3" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #3</h4>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot3.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

			<div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo4" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #4</h4>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot4.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

			<div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo5" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #5</h4>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot5.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

<<<<<<< HEAD
				<a href="http://demo3.lupo.moonbear.co" class="screenshot">
					<div class="overlay">
						<div class="overlay-center">
							<h4>Demo #3</h4>
=======
		    <div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo6" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #6</h4>
							</div>
>>>>>>> origin/master
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot6.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

<<<<<<< HEAD
				<a href="http://demo4.lupo.moonbear.co" class="screenshot">
					<div class="overlay">
						<div class="overlay-center">
							<h4>Demo #4</h4>
=======
		    <div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo7" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #7</h4>
							</div>
>>>>>>> origin/master
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot7.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>

<<<<<<< HEAD
				<a href="http://demo5.lupo.moonbear.co" class="screenshot">
					<div class="overlay">
						<div class="overlay-center">
							<h4>Demo #5</h4>
=======
		    <div class='device-container'>
		    	<div class='screen'>
				    <a href="http://lupo.moonbear.co/demo8" class="screenshot">
						<div class="overlay">
							<div class="overlay-center">
								<h4>Demo #8</h4>
							</div>
>>>>>>> origin/master
						</div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot8.jpg" alt="">
					</a>
		    	</div>
		    	<img class='base' src='<?php echo get_stylesheet_directory_uri(); ?>/assets/img/device-laptop-bottom.png' alt="">
		    </div>
		</div>
	</div>

<?php } ?>