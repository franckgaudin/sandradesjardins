<?php
/*-----------------------------------------------------------------------------------*/
/*	Standard Post
/*-----------------------------------------------------------------------------------*/

add_action('single_post', 'moonbear_single_post'); 

function moonbear_single_post() { ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* SET CONTENT WIDTH
/*-----------------------------------------------------------------------------------*/

$sidebar = get_theme_mod('blog_sidebar');

if ($sidebar == "none" || $sidebar == "") { $span_size = "col-md-8 col-md-offset-2"; }
if ($sidebar == "left" || $sidebar == "right") { $span_size = "col-md-7 col-md-offset-1"; }

?>

<?php if (has_post_thumbnail() ) { ?>
	<?php $featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' ); ?>
	<div class="header-featured-image" style="background-image: url('<?php echo $featured_image_url[0]; ?>')">
		<div class="header-meta">
			<h1 class="title"><?php the_title(); ?></h1><br>
			<span class="sub-title"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
		</div>
	</div>
<?php } ?>

<div class="single content-container">
	<div class="container">
		<div class="row">

			<!-- LEFT SIDEBAR -->
			<?php if ($sidebar == "left") { ?>
				<?php if ( is_active_sidebar(get_theme_mod('select_blog_sidebar'))) { ?>
					<div class="col-md-3 widget-area widget-left">
						<?php dynamic_sidebar(get_theme_mod('select_blog_sidebar')); ?>
					</div>
				<?php } ?>
			<?php } ?>
		
			<div class="<?php echo esc_attr($span_size); ?>">

				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php 
					global $more; $more = 0; 
					if(!get_field("post_formats")) {
						get_template_part('post/'.'standard');
					} else {
						get_template_part('post/'.get_field("post_formats"));
					}
					?>
						
				<?php endwhile; // end of the loop. ?>


				<div class="tags-share">

 					<div class="column">
 					<?php if (has_tag()) { ?>
						Tagged In:<br>
						<?php the_tags( '', '', '' ); ?> 
					<?php } ?>
					</div>

					<div class="column right">
					<?php if (get_theme_mod("social_share_buttons")) { ?>
						Share:<br>
						<?php moonbear_social_share(); ?>
					<?php } ?>
					</div>
					
				</div>

				<!-- Post Footer -->
				<?php if ( get_the_author_meta('description') ) { ?>
		   		<div class="author">
			   		<div id="authorarea">
			   			<?php echo get_avatar( get_the_author_meta( 'user_email' ), 150 ); ?>
						<h3><?php echo get_the_author(); ?></h3>
						<p>
							<?php the_author_meta( 'description' ); ?>
						</p>
					</div>
				</div>
				<?php } ?>

				<!-- Link Pages -->
				<div class="post-pagination">
					<?php moonbear_custom_wp_link_pages(); ?>
				</div>

				<div class="clearboth"></div>

	   		 	<div class="single-comments">

					<?php $comment_count = get_comment_count(); ?>
			 		<?php if ($comment_count['approved'] > 0) : ?>
				 		<?php if (comments_open()) { ?>

						<h4 class="small-title"><?php comments_number(); ?></h4>

						<?php } ?>
					<?php endif; ?>
						
					<?php 
					global $withcomments;
					$withcomments = true; 
					comments_template(); ?>

				</div>

			</div>

			<?php wp_reset_postdata(); ?>

			<!-- RIGHT SIDEBAR -->
			<?php if ($sidebar == "right") { ?>
				<?php if ( is_active_sidebar(get_theme_mod('select_blog_sidebar'))) { ?>
					<div class="col-md-3 widget-area widget-right">
						<?php dynamic_sidebar(get_theme_mod('select_blog_sidebar')); ?>
					</div>
				<?php } ?>
			<?php } ?>
				
		</div>
	
	</div> <!--END .container --> 
</div>

<?php } ?>