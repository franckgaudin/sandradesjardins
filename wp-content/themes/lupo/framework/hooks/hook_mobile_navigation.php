<?php 
/*-----------------------------------------------------------------------------------*/
/*	Mobile Navigation
/*-----------------------------------------------------------------------------------*/

add_action('mobile_navigation', 'moonbear_mobile_navigation'); 

function moonbear_mobile_navigation() { ?>

	<div class="mobile-nav-container">
		<div class="mobile-nav-bar">
			<button type="button" class="btn-mobile-nav collapsed" data-toggle="collapse" data-target="#mobile-nav"><i class="fa fa-angle-down"></i></button>

			<!-- Mobile Logo -->
			<div class="mobile-logo">
            	<?php if (get_theme_mod('logo_image')) { ?>
					<a href="<?php echo esc_url(home_url()); ?>">
						<img src="<?php echo esc_url(get_theme_mod('logo_image')); ?>" alt=""/>
					</a>
				<?php } else { ?>
					<a href="<?php echo esc_url(home_url()); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt=""/>
					</a>
				<?php } ?>

			</div>
		 	<div class="clearboth"></div>
		</div>
		
		<!-- Mobile Drop Down -->
		<div id="mobile-nav" class="collapse">
			<ul>
				<?php
				$defaults = array(
			   	'theme_location'  => 'header-menu',
			   	'container'       => 'ul',
			   	'menu_class'      => 'menu',
			   	'items_wrap'      => '%3$s',
			   	'fallback_cb' => false,
			   	'walker'          => new ctx_custom_menu_walker()
			   );
			   ?>
			   <?php wp_nav_menu( $defaults ); ?>
			</ul>
		
		
		</div>
	</div>

<?php } ?>