<?php

/*-----------------------------------------------------------------------------------*/
/*	Navigation
/*-----------------------------------------------------------------------------------*/

add_action('navigation', 'moonbear_navigation');

function moonbear_navigation() { ?>
		<div class="header-wrapper" data-sticky="<?php echo get_theme_mod('sticky_header'); ?>">
			<div class="header">
				<div class="logo">
					
						<?php if (get_theme_mod('logo_image')) { ?>
							<a href="<?php echo esc_url(home_url('/')); ?>">
								<img src="<?php echo esc_url(get_theme_mod('logo_image')); ?>" alt=""/>
							</a>
						<?php } else { ?>
							<a href="<?php echo esc_url(home_url('/')); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt=""/>
							</a>
						<?php } ?>
				</div>

				<nav>
					<ul class="navigation">
				
						<!-- Navigation Menu -->
						<?php
						if (has_nav_menu( 'header-menu' )) { 
							$header_menu = array('container' => 'ul', 'menu_class' => 'menu', 'items_wrap' => '%3$s', 'theme_location'  => 'header-menu', 'fallback_cb' => false, 'walker' => new ctx_custom_menu_walker());
							wp_nav_menu( $header_menu );
						}
						?>

					</ul>
				</nav>
				
			</div>
		</div>
<?php }