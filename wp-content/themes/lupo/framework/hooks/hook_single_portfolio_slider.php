<?php

add_action('single_portfolio_slider', 'moonbear_single_portfolio_slider'); 

function moonbear_single_portfolio_slider() { ?>

<div class="portfolio-slider">

 <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide" data-interval="<?php echo get_theme_mod('portfolio_slider_speed'); ?>" data-pause="<?php echo get_theme_mod('portfolio_slider_pause'); ?>">

        <!-- Indicators -->
        <ol class="carousel-indicators">

            <?php $row = 0; ?>
            <?php if (get_field('portfolio')) { ?>
                <?php while(has_sub_field('portfolio')) { ?>

                <li data-target="#myCarousel" data-slide-to="<?php echo $row; ?>" class="<?php if ($row == 0) { echo 'active'; } ?>"></li>

                <?php ++$row; ?>
                <?php } ?>
            <?php } ?>

        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">

        <?php $row = 1; ?>
        <?php if (get_field('portfolio')) { ?>
			<?php while(has_sub_field('portfolio')) { ?>

    		<div class="item <?php if ($row == 1) { echo 'active'; } ?>">
                <!-- Set the first background image using inline CSS below. -->
                <img src="<?php the_sub_field('image'); ?>"/>

            </div>

            <?php ++$row; ?>
			<?php } ?>
		<?php } ?>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>

    <div class="portfolio-pagination">
        <!--<a href="#" class="project-details"><i class="fa fa-info-circle"></i> &nbsp; Project Details</a>-->
    	<?php previous_post_link('%link', '<i class="fa fa-angle-left"></i>'); ?>
    	<?php next_post_link('%link', '<i class="fa fa-angle-right"></i>'); ?>
    </div>

</div>

<div class="preload-images">
            <?php if (get_field('portfolio')) { ?>
            <?php while(has_sub_field('portfolio')) { ?>
                <img src="<?php the_sub_field('image'); ?>">
            <?php } ?>
        <?php } ?>
</div>

<?php } ?>