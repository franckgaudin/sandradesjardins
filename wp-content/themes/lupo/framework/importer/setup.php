<?php

/*-----------------------------------------------------------------------------------*/
/* IMPORT PLUGINS
/*-----------------------------------------------------------------------------------*/

add_action( 'wp_ajax_moonbear_plugins', 'moonbear_plugins' );
function moonbear_plugins() 
{

	require_once ABSPATH . 'wp-admin/includes/plugin.php'; // Need for plugins_api
    require_once ABSPATH . 'wp-admin/includes/plugin-install.php'; // Need for plugins_api
    require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php'; // Need for upgrade classes
    require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader-skins.php'; // Need for upgrade classes
    require_once ABSPATH . 'wp-admin/includes/file.php'; // Need for upgrade classes
    require_once ABSPATH . 'wp-admin/includes/misc.php'; // Need for upgrade classes
    require_once ABSPATH . 'wp-admin/includes/admin.php'; // Need for upgrade classes

    // Create a new instance of Plugin_Upgrader.
    $upgrader = new Plugin_Upgrader( $skin = new Plugin_Installer_Skin( compact( 'type', 'title', 'url', 'nonce', 'plugin', 'api' ) ) );

  	// Perform the action and install the plugin from the $source urldecode().
    $wordpress_importer = $upgrader->install( "https://downloads.wordpress.org/plugin/wordpress-importer.0.6.1.zip" );
    $contact_form_7 = $upgrader->install( "https://downloads.wordpress.org/plugin/contact-form-7.4.3.1.zip" );
    $js_composer = $upgrader->install( "http://lupo.moonbear.co/plugins/js_composer.zip" );
    $moonbear = $upgrader->install( "http://lupo.moonbear.co/plugins/moonbear.zip" );

    $plugins = array('moonbear/moonbear.php', 'wordpress-importer/wordpress-importer.php', 'js_composer/js_composer.php', 'contact-form-7/wp-contact-form-7.php');
    $activate = activate_plugins( $plugins );
   
}

/*-----------------------------------------------------------------------------------*/
/* LOAD IMPORTER
/*-----------------------------------------------------------------------------------*/

function moonbear_load_importer() {
    if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

    require_once ABSPATH . 'wp-admin/includes/import.php';

    if ( ! class_exists( 'WP_Importer' ) ) {
        $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
        if ( file_exists( $class_wp_importer ) )
        {
            require $class_wp_importer;
        }
    }

    if ( ! class_exists( 'WP_Import' ) ) {
        //$class_wp_importer = get_template_directory() ."/framework/importer/wordpress-importer.php";
        $class_wp_importer = ABSPATH . 'wp-content/plugins/moonbear/importer/wordpress-importer.php';
        if ( file_exists( $class_wp_importer ) )
            require $class_wp_importer;

        //require_once( ABSPATH . '/wp-content/plugins/wordpress-importer/wordpress-importer.php' );
    }
}

/*-----------------------------------------------------------------------------------*/
/* CLOSE DEMO IMPORTER ALERT
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_ajax_moonbear_skip_setup', 'moonbear_skip_setup' );
function moonbear_skip_setup() 
{
    update_option("moonbear_setup", true);
}

/*-----------------------------------------------------------------------------------*/
/* 1.0 DEFAULT DEMO
/*-----------------------------------------------------------------------------------*/

add_action( 'wp_ajax_moonbear_import_default', 'moonbear_import_default' );
function moonbear_import_default() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
	    $ooga = new moonbear_WP_Import;
	    $ooga->fetch_attachments = true;
		$ooga->import( get_template_directory(). '/framework/importer/default.xml' );
	}
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
	update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
	set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
	set_theme_mod('logo_height', 80);
	set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'standard');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('sticky_header', true);

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo2', 'moonbear_import_demo2' );
function moonbear_import_demo2() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Crimson Text');
    set_theme_mod('grid_size', 'portfolio-2col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'fullscreen');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('hero_text_style', 'none');
    set_theme_mod('sticky_header', true);

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo3', 'moonbear_import_demo3' );
function moonbear_import_demo3() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default2.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Crimson Text');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'side');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('hero_text_style', 'none');
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_overlay_style', 'logo');
    set_theme_mod('portfolio_overlay_colour', 'rgba(0,65,86,0.7)');

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo4', 'moonbear_import_demo4' );
function moonbear_import_demo4() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'standard');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_style', 'fullscreen');

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo5', 'moonbear_import_demo5' );
function moonbear_import_demo5() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default2.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'fullscreen');
    set_theme_mod('portfolio_filter', false);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('hero_text_style', 'uppercase');
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_style', 'fullscreen');

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo6', 'moonbear_import_demo6' );
function moonbear_import_demo6() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'fullscreen');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_grid_spacing', '10');
    set_theme_mod('portfolio_overlay_style', 'logo');
    set_theme_mod('portfolio_overlay_colour', 'rgba(44,214,157,0.7)');

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo7', 'moonbear_import_demo7' );
function moonbear_import_demo7() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default3.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-3col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'fullscreen');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_grid_spacing', '3');
    set_theme_mod('portfolio_overlay_style', 'logo');
    set_theme_mod('portfolio_overlay_colour', 'rgba(44,214,157,0.7)');

    die(); // this is required to return a proper result

}

add_action( 'wp_ajax_moonbear_import_demo8', 'moonbear_import_demo8' );
function moonbear_import_demo8() 
{

    // Load Importer API
    moonbear_load_importer();

    if ( class_exists( 'WP_Importer' ) ) {
        $ooga = new moonbear_WP_Import;
        $ooga->fetch_attachments = true;
        $ooga->import( get_template_directory(). '/framework/importer/default3.xml' );
    }
    
    update_option("moonbear_setup", true);
    update_option('show_on_front', 'page');
    update_option('page_on_front', '15');
    set_theme_mod('nav_menu_locations', array ( "header-menu" => 25 ));
    set_theme_mod('footer_copyright', 'LUPO WORDPRESS THEME CREATED WITH LOVE BY MOONBEAR');
    set_theme_mod('logo_height', 80);
    set_theme_mod('body_font', 'Raleway');
    set_theme_mod('heading_font', 'Raleway');
    set_theme_mod('navigation_font', 'Raleway');
    set_theme_mod('hero_font', 'Oswald');
    set_theme_mod('grid_size', 'portfolio-2col');
    set_theme_mod('footer_logo', true);
    set_theme_mod('footer_about_text', '<h2>We are an award winning digital agency</h2><p>My lord! you’re a tripod. a drug person can learn to cope with things like seeing their dead grandmother crawling up their leg with a knife in her teeth.</p>');
    set_theme_mod('social_footer_buttons', array("facebook", "twitter", "instagram", "youtube"));
    set_theme_mod('navigation_style', 'standard');
    set_theme_mod('portfolio_filter', true);
    set_theme_mod('portfolio_per_page', 11);
    set_theme_mod('sticky_header', true);
    set_theme_mod('portfolio_grid_spacing', '8');
    set_theme_mod('portfolio_overlay_style', 'logo');
    set_theme_mod('portfolio_overlay_colour', 'rgba(255,53,53,0.7)');

    die(); // this is required to return a proper result

}