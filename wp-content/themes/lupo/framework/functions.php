<?php

/*-----------------------------------------------------------------------------------

    THEME FUNCTIONS
    -----------------------------------------------------------------------------

	Editing this file will break the theme and possibly the universe.


    TABLE OF CONTENTS
    -----------------------------------------------------------------------------

    1. 	Set Content Width
    2. 	Add Theme Support
    3. 	Load Admin JS Files
    4. 	Load theme CSS
    5. 	Load Admin CSS
    6. 	Load JS Files
    7. 	Register Menu Areas
    8.  Register Widget Areas
    9.  Other misc functions

/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	1. Set Content Width
/*-----------------------------------------------------------------------------------*/

if ( ! isset( $content_width ) )
	$content_width = 634;


/*-----------------------------------------------------------------------------------*/
/*	2. Add Theme Support
/*-----------------------------------------------------------------------------------*/

add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails');
add_theme_support( 'custom-header');
add_theme_support( 'custom-background');
add_theme_support( 'title-tag' );

/*-----------------------------------------------------------------------------------*/
/*	3. Load Admin JS Files
/*-----------------------------------------------------------------------------------*/

function moonbear_admin_scripts() {
	wp_register_script( 'moonbear-admin', get_template_directory_uri() . '/framework/admin/js/admin.js' );
	wp_enqueue_script( 'moonbear-admin' );
}

add_action( 'admin_enqueue_scripts', 'moonbear_admin_scripts' );

/*-----------------------------------------------------------------------------------*/
/*	4. Load Theme CSS Files
/*-----------------------------------------------------------------------------------*/

function moonbear_styles()  {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array());
	wp_enqueue_style( 'bootstrap-custom', get_template_directory_uri() . '/assets/css/bootstrap-custom.css', array());
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style( 'elements', get_template_directory_uri() . '/assets/css/elements.css');
	wp_enqueue_style( 'scripts', get_template_directory_uri() . '/assets/css/scripts.css', array());
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css', array());
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css', array());
	wp_enqueue_style( 'select', get_template_directory_uri() . '/assets/css/select.css', array());
}

add_action( 'wp_enqueue_scripts', 'moonbear_styles' );

/*-----------------------------------------------------------------------------------*/
/*	5. Load Admin CSS Files
/*-----------------------------------------------------------------------------------*/

function moonbear_admin_styles() {
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/framework/admin/css/admin.css', array());
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css', array());
}

add_action( 'admin_enqueue_scripts', 'moonbear_admin_styles' );

/*-----------------------------------------------------------------------------------*/
/*	6. Load Javascript Files
/*-----------------------------------------------------------------------------------*/

function moonbear_theme_scripts() {
	wp_enqueue_script('jquery-ui-widget');
	wp_enqueue_script( 'comment-reply' );
    wp_enqueue_script( 'flex-slider', get_template_directory_uri() . "/assets/js/jquery.flexslider-min.js", array('jquery'),'', 'in_footer');
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . "/assets/js/bootstrap.min.js", array('jquery'),'');
    wp_enqueue_script( 'select', get_template_directory_uri().'/assets/js/select.js', array('jquery'),'', 'in_footer');
    wp_enqueue_script( 'isotope', get_template_directory_uri().'/assets/js/jquery.isotope.min.js', array('jquery'), 'in_footer');
    wp_enqueue_script( 'infinite-scroll', get_template_directory_uri().'/assets/js/jquery.infinitescroll.min.js', array('jquery'), 'in_footer');
    wp_enqueue_script( 'manual-trigger', get_template_directory_uri().'/assets/js/jquery.manual-trigger.js', array('jquery'), 'in_footer');
    wp_enqueue_script( 'images-loaded', get_template_directory_uri().'/assets/js/imagesloaded.pkgd.min.js', array('jquery'), 'in_footer');
    wp_enqueue_script( 'theme-custom', get_template_directory_uri().'/assets/js/theme-custom.js', array('jquery'), 'in_footer');
    wp_enqueue_script( 'twitter_feed', get_template_directory_uri().'/framework/twitter/jquery.tweet.js', array('jquery'),'', 'in_footer');
    wp_enqueue_script( 'jquery-mobile', get_template_directory_uri().'/assets/js/jquery.mobile.js', array('jquery'),'', 'in_footer');
}
add_action( 'wp_enqueue_scripts', 'moonbear_theme_scripts' );

/*-----------------------------------------------------------------------------------*/
/*	8. Load Google Fonts
/*-----------------------------------------------------------------------------------*/

function moonbear_slugify($string){
   $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   return $slug;
}

function moonbear_google_fonts() {

	$protocol = is_ssl() ? 'https' : 'http';

	$body_font = get_theme_mod('body_font');
	$heading_font = get_theme_mod('heading_font');
	$navigation_font = get_theme_mod('navigation_font');
	$hero_font = get_theme_mod('hero_font');

	wp_enqueue_style( "Raleway", "$protocol://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" );

	if ($body_font) {
	    wp_enqueue_style( moonbear_slugify($body_font), "$protocol://fonts.googleapis.com/css?family=".$body_font.":200,300,400,500,600,700" );
	} else {
		wp_enqueue_style( "Raleway", "$protocol://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" );
	}
	if ($heading_font) {
    	wp_enqueue_style( moonbear_slugify($heading_font), "$protocol://fonts.googleapis.com/css?family=".$heading_font.":200,300,400,500,600,700" );
    }
    if ($navigation_font) {
    	wp_enqueue_style( moonbear_slugify($navigation_font), "$protocol://fonts.googleapis.com/css?family=".$navigation_font.":200,300,400,500,600,700" );
    }
    if ($hero_font) {
    	wp_enqueue_style( moonbear_slugify($hero_font), "$protocol://fonts.googleapis.com/css?family=".$hero_font.":200,300,400,500,600,700" );
    } else {
    	wp_enqueue_style( "Oswald", "$protocol://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" );
    }

}
add_action( 'wp_enqueue_scripts', 'moonbear_google_fonts' );

/*-----------------------------------------------------------------------------------*/
/*	7. Register Menu Areas
/*-----------------------------------------------------------------------------------*/

function moonbear_register_menu() {
  register_nav_menus(
    array( 'header-menu' => 'Header Menu' )
  );
}

add_action( 'init', 'moonbear_register_menu' );

/*-----------------------------------------------------------------------------------*/
/*	8. Register Widget Areas
/*-----------------------------------------------------------------------------------*/

$widget_footer_columns = get_theme_mod('widget_footer_columns');

// Sidebar


add_action( 'widgets_init', 'moonbear_widgets_init' );
function moonbear_widgets_init() {
	$widget_area_1 = array(
	'name'          => 'Widget Area 1',
	'id'			=> 'widget-area-1',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

// Sidebar
$widget_area_2 = array(
	'name'          => 'Widget Area 2',
	'id'			=> 'widget-area-2',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

// Sidebar
$widget_area_3 = array(
	'name'          => 'Widget Area 3',
	'id'			=> 'widget-area-3',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

// Sidebar
$widget_area_4 = array(
	'name'          => 'Widget Area 4',
	'id'			=> 'widget-area-4',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

// Sidebar
$widget_area_5 = array(
	'name'          => 'Widget Area 5',
	'id'			=> 'widget-area-5',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

// Sidebar
$widget_area_6 = array(
	'name'          => 'Widget Area 6',
	'id'			=> 'widget-area-6',
	'description'	=> 'This is the standard sidebar widget area. Place widgets on here to display them on the sidebar of pages. Please activate the sidebar through the page editor for the pages you want it be displayed on.',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );

    register_sidebar($widget_area_1);
	register_sidebar($widget_area_2);
	register_sidebar($widget_area_3);
	register_sidebar($widget_area_4);
	register_sidebar($widget_area_5);
	register_sidebar($widget_area_6);
}

/*-----------------------------------------------------------------------------------*/
/*	9. OTHER MISC FUNCTIONS
/*-----------------------------------------------------------------------------------*/

class ctx_custom_menu_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth = 0, $args = array(), $current_object_id = 0)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           if ($item->megamenu == 1) {
	           $megamenu = "mega-menu";
           } else {
	           $megamenu = "";
	          }

			if ($item->megamenu_2col == 1) {
			$megamenu_2col = "mega-menu-2col";
			} else {
			$megamenu_2col = "";
			}

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. $megamenu .' '.$megamenu_2col .' '. esc_attr( $class_names ) . '"';

         //  $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
           $output .= $indent . '<li' . $value . $class_names .'>';

           $attributes = ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '';
           $append = '';

           $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';


            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';

            if ($item->attr_title) {
            	$item_output .= '<i class="fa fa-'.esc_attr( $item->attr_title ).'"></i>';
            }
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $description.$args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
}

function moonbear_acf_load_sidebar( $field ) {
	$field['choices'] = array();
	$field['choices']['widget-area-1'] = 'Widget Area 1';
	$field['choices']['widget-area-2'] = 'Widget Area 2';
	$field['choices']['widget-area-3'] = 'Widget Area 3';
	$field['choices']['widget-area-4'] = 'Widget Area 4';
	$field['choices']['widget-area-5'] = 'Widget Area 5';
	$field['choices']['widget-area-6'] = 'Widget Area 6';

	return $field;
}

add_filter('acf/load_field/name=sidebars', 'moonbear_acf_load_sidebar');

/*-----------------------------------------------------------------------------------*/
/*	Custom Search Output
/*-----------------------------------------------------------------------------------*/

function moonbear_html5_search_form( $form ) {
    $form = '<form method="get" id="searchform" action="' . esc_url(home_url( '/' )) . '" >
    <input type="text" placeholder="Search" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'moonbear_html5_search_form' );


/*-----------------------------------------------------------------------------------*/
/*	Custom Comment Styling
/*-----------------------------------------------------------------------------------*/

function moonbear_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	} ?>

	<?php if ( 'div' != $args['style'] ) : ?>
	<li id="div-comment-<?php comment_ID(); ?>">
	<?php endif; ?>
		<div class="entry">
			<div class="avatar">
				<?php echo get_avatar( get_comment_author_email(), '', '', ''); ?>
			</div>
			<div class="comment-meta">
				<h4><?php comment_author_link(); ?></h4>

			</div>
			<div class="comment-body">
		  		<?php if ($comment->comment_approved == '0') : ?>
		  			<div class="comment-approval">Your comment is awaiting approval</div>
		  		<?php endif; ?>
		  		<?php comment_text(); ?>

		  		<div class="comment-footer">
		  		<span> <?php edit_comment_link('Edit','  ','' ); ?></span>
		  		<span> <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
		  		<span> <?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?></span>

		  		</div>
		  	</div>

		</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</li>
	<?php endif; ?>
<?php
}

/*-----------------------------------------------------------------------------------*/
/*	Remove thumbnail dimensions
/*-----------------------------------------------------------------------------------*/

function moonbear_remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

add_filter( 'post_thumbnail_html', 'moonbear_remove_thumbnail_dimensions', 10, 3 );

/*-----------------------------------------------------------------------------------*/
/*	Custom wp_link_pages
/*-----------------------------------------------------------------------------------*/

function moonbear_custom_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before' => '<ul class="pagination">',
		'after' => '</ul>',
		'text_before' => '',
		'text_after' => '',
		'next_or_number' => 'number',
		'nextpagelink' => 'Next page',
		'previouspagelink' => 'Previous page',
		'pagelink' => '%',
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				$output .= '<li>';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current-post-page">';

				$output .= $text_before . $j . $text_after;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
				$output .= '</li>';
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $previouspagelink . $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}

/*-----------------------------------------------------------------------------------*/
/*	Add dropdown class
/*-----------------------------------------------------------------------------------*/

function moonbear_menu_set_dropdown( $sorted_menu_items, $args ) {
    $last_top = 0;
    foreach ( $sorted_menu_items as $key => $obj ) {
        // it is a top lv item?
        if ( 0 == $obj->menu_item_parent ) {
            // set the key of the parent
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'dropdown';
        }
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'moonbear_menu_set_dropdown', 10, 2 );

/*-----------------------------------------------------------------------------------*/
/*	Custom Pagination
/*-----------------------------------------------------------------------------------*/

function moonbear_custom_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text'    => '<i class="fa fa-angle-left"></i>',
			'next_text'    => '<i class="fa fa-angle-right"></i>',
        ) );
        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            foreach ( $pages as $page ) {
                    echo "<li>$page</li>";
            }
        }
}

add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title )
{
  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
    return bloginfo('name') . ' | ' . get_bloginfo( 'description' );
  }
  return $title;
}

/*-----------------------------------------------------------------------------------*/
/*	Social Share Buttons
/*-----------------------------------------------------------------------------------*/

function moonbear_social_share() {
	$social_share_buttons = get_theme_mod("social_share_buttons"); ?>
	<?php if ($social_share_buttons) { ?>
		<?php foreach ($social_share_buttons as $social_share_button) { ?>
			<?php if ($social_share_button == "facebook") { $share_link = "https://www.facebook.com/sharer/sharer.php?u=".get_the_permalink(); } ?>
			<?php if ($social_share_button == "twitter") { $share_link = "https://twitter.com/home?status=".get_the_permalink(); } ?>
			<?php if ($social_share_button == "google") { $share_link = "https://plus.google.com/share?url=".get_the_permalink(); } ?>
			<?php if ($social_share_button == "linkedin") { $share_link = "https://www.linkedin.com/shareArticle?mini=true&url=".get_the_permalink()."&title=".get_the_title()."&summary=&source="; } ?>
			<?php if ($social_share_button == "pinterest") { $share_link = "https://pinterest.com/pin/create/button/?url=".$featured_image_url[0]."&media=".get_the_title()."&description=";} ?>
			<a target="_blank" href="<?php echo esc_url($share_link); ?>"><i class="fa fa-<?php echo esc_html($social_share_button); ?>"></i></a>
		<?php } ?>
	<?php } ?>
<?php
}

/*-----------------------------------------------------------------------------------*/
/*	Add Read More to excerpt
/*-----------------------------------------------------------------------------------*/

function moonbear_new_excerpt_more($more) {
	global $post;
	return '';
}
add_filter('excerpt_more', 'moonbear_new_excerpt_more');

function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}

/*-----------------------------------------------------------------------------------*/
/*	SEO
/*-----------------------------------------------------------------------------------*/

function moonbear_insert_image_src_rel_in_head() {
	global $post;
	if ( !is_singular()) { //if it is not a post or a page
		return;
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
		echo '<meta property="og:title" content="'.get_the_title($post->ID).'"/>';
		echo '<meta property="og:description" content="'.get_post_field('post_content', $post->ID).'"/>';

	}
}
add_action( 'wp_head', 'moonbear_insert_image_src_rel_in_head', 5 );

/*-----------------------------------------------------------------------------------*/
/*	Social Footer Links
/*-----------------------------------------------------------------------------------*/

function social_footer_buttons() {
	if (get_theme_mod("social_footer_buttons")) {
		$social_footer_buttons = get_theme_mod("social_footer_buttons") ?>
		<?php foreach ($social_footer_buttons as $social_footer_button) { ?>

			<?php if ($social_footer_button == "facebook") { $social_page = get_theme_mod("facebook_profile"); } ?>
			<?php if ($social_footer_button == "twitter") { $social_page = get_theme_mod("twitter_profile"); } ?>
			<?php if ($social_footer_button == "google") { $social_page = get_theme_mod("google_profile"); } ?>
			<?php if ($social_footer_button == "instagram") { $social_page = get_theme_mod("instagram_profile"); } ?>
			<?php if ($social_footer_button == "linkedin") { $social_page = get_theme_mod("linkedin_profile"); } ?>
			<?php if ($social_footer_button == "pinterest") { $social_page = get_theme_mod("pinterest_profile"); } ?>
			<?php if ($social_footer_button == "youtube") { $social_page = get_theme_mod("youtube_profile"); } ?>
			<?php if ($social_footer_button == "vine") { $social_page = get_theme_mod("vine_profile"); } ?>
			<?php if ($social_footer_button == "vemeo") { $social_page = get_theme_mod("vimeo_profile"); } ?>

		<a title="Follow us on <?php echo $social_footer_button; ?>" href="<?php echo $social_page; ?>" target="_blank"><i class="fa fa-<?php echo $social_footer_button; ?>"></i> <span class="hidden-xs"><?php echo $social_footer_button; ?></span></a>
		<?php } ?>
	<?php }
}

/*-----------------------------------------------------------------------------------*/
/*	Instagram
/*-----------------------------------------------------------------------------------*/

function moonbear_modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['instagram'] = 'Instagram Numerical User ID (Use this http://jelled.com/instagram/lookup-user-id to look up your Instagram Numerical USER ID)';

	return $profile_fields;
}
add_filter('user_contactmethods', 'moonbear_modify_contact_methods');

/*-----------------------------------------------------------------------------------*/
/*	Excerpt
/*-----------------------------------------------------------------------------------*/

function moonbear_custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'moonbear_custom_excerpt_length', 999 );

/*-----------------------------------------------------------------------------------*/
/*	Demo Import
/*-----------------------------------------------------------------------------------*/

add_action( 'admin_menu', 'moonbear_demo_import_menu' );

function moonbear_demo_import_menu() {
	add_theme_page( 'Demo Installer', 'Demo Installer', 'edit_theme_options', 'moonbear-demo-installer', 'moonbear_demo_install_page'  );
}

function moonbear_demo_install_page(){
	?>
	<div class="wrap">

		<div class="demo-header">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png">
			<h2>One Click Demo Setup</h2>
		</div>
		<div class="demo-block-container">
		<div class="demo-block">
			<h3>Default</h3>
			<a href="http://lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot1.jpg"></a>
			<a data-toggle="tooltip" data-import="default" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #2</h3>
			<a href="http://demo2.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot2.jpg"></a>
			<a data-toggle="tooltip" data-import="demo2" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #3</h3>
			<a href="http://demo3.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot3.jpg"></a>
			<a data-toggle="tooltip" data-import="demo3" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #4</h3>
			<a href="http://demo4.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot4.jpg"></a>
			<a data-toggle="tooltip" data-import="demo4" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #5</h3>
			<a href="http://demo5.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot5.jpg"></a>
			<a data-toggle="tooltip" data-import="demo5" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #6</h3>
			<a href="http://demo5.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot6.jpg"></a>
			<a data-toggle="tooltip" data-import="demo6" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #7</h3>
			<a href="http://demo7.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot7.jpg"></a>
			<a data-toggle="tooltip" data-import="demo7" class="start-import button button-primary button-hero">Install</a>
		</div>

		<div class="demo-block">
			<h3>Demo #8</h3>
			<a href="http://demo7.lupo.moonbear.co"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/screenshot8.jpg"></a>
			<a data-toggle="tooltip" data-import="demo8" class="start-import button button-primary button-hero">Install</a>
		</div>

		</div>
	</div>
	<?php
}

/*-----------------------------------------------------------------------------------*/
/*	Category Filter Walker
/*-----------------------------------------------------------------------------------*/

class moonbear_category_filter_walker extends Walker_Category {

	function start_el(&$output, $category, $depth = 0, $args = "", $current_object_id = 0) {
		extract($args);

		$cat_name = esc_attr( $category->name );
		$cat_name = apply_filters( 'list_cats', $cat_name, $category );

		$link = '<a href="#" data-filter=".' . $category->slug . '">' . $cat_name . '</a>';

		if ( 'list' == $args['style'] ) {
			if ( !empty($current_category) ) {
				$_current_category = get_term( $current_category, $category->taxonomy );
				if ( $category->term_id == $current_category )
					$class .=  ' current-cat';
				elseif ( $category->term_id == $_current_category->parent )
					$class .=  ' current-cat-parent';
			}

			$output .= "<li class=" . $class;
			$output .=  '>'.$link;
			$output .=  '';
		} else {
			$output .= $link;
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Get custom category slug name
/*-----------------------------------------------------------------------------------*/

function moonbear_get_custom_category_slug($id, $category) {
	$my_terms = get_the_terms( $id, $category );

	if( $my_terms && !is_wp_error( $my_terms ) ) {
		foreach( $my_terms as $term ) {
			echo $term->slug . ' ';
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Redirect after activation
/*-----------------------------------------------------------------------------------*/

if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
	header( 'Location: '.admin_url().'admin.php?page=moonbear-demo-installer');
}

/*-----------------------------------------------------------------------------------*/
/*	Kirki Framework
/*-----------------------------------------------------------------------------------*/

include_once( dirname( __FILE__ ) . '/kirki/kirki.php' );

function moonbear_kirki( $config ) {

    $config['url_path'] = get_stylesheet_directory_uri() . '/framework/kirki/';
    return $config;

}
add_filter( 'kirki/config', 'moonbear_kirki' );

include("advanced-custom-fields/acf.php");
include("advanced-custom-fields/acf-fields.php");

?>
