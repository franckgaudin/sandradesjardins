<div class="header-wrapper" data-sticky="<?php echo get_theme_mod('sticky_header'); ?>">
	<div class="header container">
		<div class="logo">
			
				<?php if (get_theme_mod('logo_image')) { ?>
					<a href="<?php echo esc_url(home_url('/')); ?>">
						<img src="<?php echo esc_url(get_theme_mod('logo_image')); ?>" alt=""/>
					</a>
				<?php } else { ?>
					<a href="<?php echo esc_url(home_url('/')); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt=""/>
					</a>
				<?php } ?>
		</div>

		<nav>
			<ul class="navigation">
		
				<li><a class="navigation-toggle navigation-side-toggle" href="#"><i class="fa fa-bars"></i></a></li>

			</ul>
		</nav>
		
	</div>
</div>

<div class="navigation-overlay navigation-side">

	<div class="close-button"><span class="close thick"></span></div>

	<div class="vertical-center">
		<div class="logo">
			<?php if (get_theme_mod('light_logo_image')) { ?>
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php echo esc_url(get_theme_mod('light_logo_image')); ?>" alt=""/>
				</a>
			<?php } else { ?>
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_white.png" alt=""/>
				</a>
			<?php } ?>
		</div>
		<ul>
			<?php
			if (has_nav_menu( 'header-menu' )) { 
				$header_menu = array('container' => 'ul', 'menu_class' => 'menu', 'items_wrap' => '%3$s', 'theme_location'  => 'header-menu', 'fallback_cb' => false, 'walker' => new ctx_custom_menu_walker());
				wp_nav_menu( $header_menu );
			}
			?>
		</ul>
		<div class="social-buttons">
				<?php social_footer_buttons(); ?>
		</div>
	</div>
</div>