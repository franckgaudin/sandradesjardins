<div class="overlay">
	<div class="overlay-center">
		<?php if (get_theme_mod('light_logo_image')) { ?>
			<img class="overlay-logo" src="<?php echo esc_url(get_theme_mod('light_logo_image')); ?>" alt=""/>
		<?php } else { ?>
			<img class="overlay-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_white.png" alt=""/>
		<?php } ?>
	</div>
</div>