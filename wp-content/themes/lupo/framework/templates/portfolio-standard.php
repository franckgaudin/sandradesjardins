<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<?php if (get_field('hero_text')) { ?>
					<div class="hero-container">
						<?php the_field("hero_text"); ?>
					</div>
				<?php } ?>

				<?php if (get_theme_mod('portfolio_filter')) { ?>
					<ul class="category-filter">
						<?php  $category_filter_walker = new moonbear_category_filter_walker(); ?>
						<?php $args = array(
							'show_option_all'    => '',
							'orderby'            => 'name',
							'order'              => 'ASC',
							'style'              => 'list',
							'show_count'         => 0,
							'hide_empty'         => 1,
							'use_desc_for_title' => 1,
							'child_of'           => 0,
							'hierarchical'       => 1,
							'show_option_none'   => 'No categories',
							'number'             => null,
							'echo'               => 1,
							'title_li'           => '',
							'depth'              => 0,
							'current_category'   => 0,
							'pad_counts'         => 0,
							'taxonomy'           => 'portfolio_categories',
							'walker'             => $category_filter_walker
						);
						?>
						<li><a href="#" data-filter="" class="selected">Tous</a></li>
						<?php wp_list_categories($args); ?>
					</ul>
				<?php } ?>

				<div id="posts" class="isotope-container <?php echo get_theme_mod('grid_size'); ?>">
					<?php while ( have_posts() ) : the_post();
						global $more; $more = 0; ?>

						<a href="<?php the_permalink(); ?>" class="portfolio-entry image-overlay isotope-item <?php moonbear_get_custom_category_slug($post->ID, 'portfolio_categories'); ?> <?php if (get_field('secondary_image')) { echo 'portfolio-hover'; } ?>">

							<?php if (get_theme_mod('portfolio_overlay_style')) { ?>
							   <?php get_template_part('framework/templates/overlay-'.get_theme_mod('portfolio_overlay_style')); ?>
							<?php } else { ?>
							    <?php get_template_part('framework/templates/overlay-logo-title'); ?>
							<?php } ?>

							<?php if (get_field('secondary_image')) { ?>
							<div class="secondary">
								<img src="<?php the_field('secondary_image'); ?>" alt="">
							</div>
							<?php } ?>
							<?php if (get_field('primary_image')) { ?>
							<div class="primary">
								<img src="<?php the_field('primary_image'); ?>" alt="">
							</div>
							<?php } ?>
						</a>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>
