<?php get_header(); ?>

<div class="content-container">
	<div class="container">
		<div class="row">
				<div class="hero-container">
					<h1>Page not found</h1>
					<p><a href="<?php echo esc_url(home_url()); ?>">Click here</a> to go back to the homepage</p>
				</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>