<?php
/*-----------------------------------------------------------------------------------*/
/* GET THEME HEADER
/*-----------------------------------------------------------------------------------*/

get_header();

/*-----------------------------------------------------------------------------------*/
/* LOAD CONTENT
/*-----------------------------------------------------------------------------------*/ ?>
<?php  $post_count = $wp_query->found_posts; ?>
<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
				<div class="hero-container">
					<h1>Search</h1>
					<?php if ($post_count == 0) { ?>
					<p>Sorry, we didn't find anything for <?php the_search_query() ?>. Try using another term:</p>
					<form id="searchform" action="?s=" method="get" role="search">
						<input id="s" type="text" name="s" value="" placeholder="Search">
					</form>
				<?php } ?>
				</div>

				<div id="posts" class="isotope-container">	
					<?php while ( have_posts() ) : the_post();
						global $more; $more = 0; global $excerpt_length; $excerpt_length = 20; ?>
						<div class="isotope-item col-md-4">
							<?php if(!get_post_format()) {
								get_template_part('post/'.'standard');
							} else {
								get_template_part('post/'.get_post_format());
							} ?>
						</div>
					<?php endwhile; ?>
				</div>

			</div>
		</div>
	</div>
</div>
<?php 

/*-----------------------------------------------------------------------------------*/
/* WORDPRESS RESET QUERY LOOP
/*-----------------------------------------------------------------------------------*/

wp_reset_query();

/*-----------------------------------------------------------------------------------*/
/* GET FOOTER
/*-----------------------------------------------------------------------------------*/

get_footer(); 

?>  