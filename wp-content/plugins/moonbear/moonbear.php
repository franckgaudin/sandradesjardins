<?php
/**
 * Plugin Name: Moonbear
 * Plugin URI: http://www.moonbear.co
 * Description: The MoonBear Plugin
 * Version: 1.0
 * Author: MoonBear
 * Author URI: http://www.moonbear.co
 */

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Portfolio' ),
        'singular_name' => __( 'Portfolio' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'categories' ),
    )
  );
register_taxonomy("portfolio_categories", array("portfolio"), array("hierarchical" => true, "label" => "Portfolio Categories", "singular_label" => "Portfolio Category", "rewrite" => true));

}